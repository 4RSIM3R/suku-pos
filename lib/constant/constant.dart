import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:toko_ku/constant/routing.dart';

const Color white = Colors.white;
const Color black = Colors.black;
const Color grey = Colors.black54;
const Color red = Colors.red;
const Color blue = Color(0xff342ead);
const Color orange = Color(0xffea6227);
const Color yellow = Color(0xfff2a51a);

// Menu

List<String> litsTitleMenu = [
  'Manajemen\nUser',
  'Manajemen Barang',
  'Penjualan',
  'Manajemen Member',
  'Order\nBarang',
  'Laporan',
  'Setting\nPoint',
  'Store\nSetting'
];

List<String> listPagesRoutes = [
  manageUser,
  itemManagementPage,
  addTransaction,
  listMember,
  soon,
  soon,
  settingPoint,
  soon
];

List<IconData> listIconMenu = [
  Icons.supervised_user_circle,
  MdiIcons.cart,
  MdiIcons.sale,
  MdiIcons.accountBox,
  MdiIcons.plusCircle,
  MdiIcons.chartBar,
  MdiIcons.starFourPoints,
  MdiIcons.tableSettings
];
