import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toko_ku/component/typhography.dart';
import 'package:toko_ku/constant/constant.dart';
import 'package:toko_ku/constant/routing.dart';
import 'package:toko_ku/view/home.dart';
import 'package:toko_ku/view/item/add_item.dart';
import 'package:toko_ku/view/item/add_item_setting.dart';
import 'package:toko_ku/view/item/item_management.dart';
import 'package:toko_ku/view/license.dart';
import 'package:toko_ku/view/member_transaction/add_transaction_member.dart';
import 'package:toko_ku/view/member_transaction/cart_member.dart';
import 'package:toko_ku/view/member_transaction/payment_member.dart';
import 'package:toko_ku/view/setting_point/add_point.dart';
import 'package:toko_ku/view/setting_point/setting_point.dart';
import 'package:toko_ku/view/splash.dart';
import 'package:toko_ku/view/transaction/add_transaction.dart';
import 'package:toko_ku/view/transaction/cart_page.dart';
import 'package:toko_ku/view/transaction/list_transaction.dart';
import 'package:toko_ku/view/transaction/user_payment.dart';
import 'package:toko_ku/view/user/add_user.dart';
import 'package:toko_ku/view/user/list_member.dart';
import 'package:toko_ku/view/user/manage_user.dart';
import 'package:toko_ku/view/widgets/edit_member.dart';
import 'package:toko_ku/view/widgets/edit_supplier.dart';
import 'package:toko_ku/view/widgets/soon_page.dart';

class Routes {
  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case splashPage:
        return MaterialPageRoute(builder: (_) => Splash());
        break;
      case licensePage:
        return MaterialPageRoute(builder: (_) => License());
        break;
      case homePage:
        return MaterialPageRoute(builder: (_) => Home());
        break;
      case itemManagementPage:
        return MaterialPageRoute(builder: (_) => ItemManagement());
        break;
      case addItem:
        return MaterialPageRoute(builder: (_) => AddItem());
        break;
      case addItemSetting:
        return MaterialPageRoute(builder: (_) => AddItemSetting());
      case addTransaction:
        return MaterialPageRoute(builder: (_) => AddTransaction());
        break;
      case cartPage:
        return MaterialPageRoute(builder: (_) => CartPage());
        break;
      case userPayment:
        return MaterialPageRoute(builder: (_) => UserPayment());
        break;
      case listMember:
        return MaterialPageRoute(builder: (_) => ListMember());
        break;
      case manageUser:
        return MaterialPageRoute(builder: (_) => ManageUser());
        break;
      case addUser:
        AddUserArgument argument = settings.arguments;
        return MaterialPageRoute(builder: (_) => AddUser(argument: argument));
        break;
      case editMember:
        EditMemberArgument argument = settings.arguments;
        return MaterialPageRoute(builder: (_) => EditMember(argument: argument));
        break;
      case editSupplier:
        EditSupplierArgument argument = settings.arguments;
        return MaterialPageRoute(builder: (_) => EditSupplier(arg: argument));
        break;
      case listTransaction:
        return MaterialPageRoute(builder: (_) => ListTransaction());
        break;
      case addTransactionMember:
        return MaterialPageRoute(builder: (_) => AddTransactionMember());
        break;
      case cartMember:
        return MaterialPageRoute(builder: (_) => CartMember());
        break;
      case settingPoint:
        return MaterialPageRoute(builder: (_) => SettingPoint());
      case addPoint:
        return MaterialPageRoute(builder: (_) => AddPoint());
      case paymentMember:
        return MaterialPageRoute(builder: (_) => PaymentMember());
      case soon:
        return MaterialPageRoute(builder: (_) => SoonPage());
        break;
      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            backgroundColor: white,
            body: Center(
              child: heading1(text: '404 Page Not Found'),
            ),
          ),
        );
    }
  }
}
