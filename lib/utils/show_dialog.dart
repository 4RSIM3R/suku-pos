import 'package:flutter/material.dart';

void showCommonDialog({
  @required BuildContext context,
  @required Widget content,
  @required Widget title,
  @required List<Widget> actions,
}) {
  showDialog(
    context: context,
    barrierDismissible: true,
    builder: (dialogContext) => AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      title: title,
      content: content,
      actions: actions,
    ),
  );
}
