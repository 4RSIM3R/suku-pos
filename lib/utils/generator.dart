import 'package:flutter/material.dart';
import 'package:toko_ku/constant/constant.dart';

Color colorGeneratorList(int index){
  if (index % 2 == 0) {
    return yellow;
  } else if (index % 3 == 0) {
    return orange;
  }
  return blue;
}