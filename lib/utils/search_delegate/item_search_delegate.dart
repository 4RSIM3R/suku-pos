import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:provider/provider.dart';
import 'package:toko_ku/bloc/local/transaction_bloc.dart';
import 'package:toko_ku/component/box.dart';
import 'package:toko_ku/component/typhography.dart';
import 'package:toko_ku/service/database/core_database.dart';

class ItemSearchDelegate extends SearchDelegate {
  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    final db = Provider.of<CoreDatabase>(context);
    final transactionBloc = Provider.of<TransactionBloc>(context);
    return StreamBuilder<List<Item>>(
      stream: db.itemDao.searchItem(query),
      builder: (context, AsyncSnapshot<List<Item>> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        } else if (snapshot.data.length == 0) {
          return Container(
            child: Center(
              child: heading3(text: 'Item Tidak Ditemukan'),
            ),
          );
        }
        return Container(
          padding: EdgeInsets.all(12.0),
          child: StaggeredGridView.countBuilder(
            physics: BouncingScrollPhysics(),
            crossAxisCount: 4,
            itemCount: snapshot.data.length,
            itemBuilder: (BuildContext context, int index) => GestureDetector(
              onTap: () {
                transactionBloc.addItem(snapshot.data[index]);
                Navigator.of(context).pop();
              },
              child: BoxForItem(
                item: snapshot.data[index],
              ),
            ),
            staggeredTileBuilder: (int index) => new StaggeredTile.count(2, 3),
            mainAxisSpacing: 8.0,
            crossAxisSpacing: 8.0,
          ),
        );
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // TODO: implement buildSuggestions
    return Container();
  }
}
