import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:toko_ku/component/typhography.dart';
import 'package:toko_ku/service/database/core_database.dart';

class SupplierSearchDelegate extends SearchDelegate {
  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    final db = Provider.of<CoreDatabase>(context);
    return StreamBuilder<List<Supplier>>(
      stream: db.supplierDao.searchSupplier(query),
      builder: (context, AsyncSnapshot<List<Supplier>> snapshot) {
        print('Error ${snapshot.error} |  Query $query');
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        } else if (snapshot.data.length == 0) {
          return Container(
            child: Center(
              child: heading3(text: 'Supplier Tidak Ditemukan'),
            ),
          );
        }
        return Container(
          padding: EdgeInsets.all(12.0),
          child: ListView.builder(
            physics: BouncingScrollPhysics(),
            itemCount: snapshot.data.length,
            itemBuilder: (context, index) {
              return ListTile(
                title: heading5(text: snapshot.data[index].name),
              );
            },
          ),
        );
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // TODO: implement buildSuggestions
    return Container();
  }
}
