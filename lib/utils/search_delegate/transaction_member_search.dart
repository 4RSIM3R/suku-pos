import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:toko_ku/bloc/local/member_transaction_bloc.dart';
import 'package:toko_ku/component/typhography.dart';
import 'package:toko_ku/service/database/core_database.dart';

class TransactionMemberSearch extends SearchDelegate {
  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    final db = Provider.of<CoreDatabase>(context);
    final bloc = Provider.of<MemberTransactionBloc>(context);
    return StreamBuilder<List<Member>>(
      stream: db.memberDao.searchMember(query),
      builder: (context, AsyncSnapshot<List<Member>> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        } else if (snapshot.data.length == 0) {
          return Container(
            child: Center(
              child: heading3(text: 'Member Tidak Ditemukan'),
            ),
          );
        }
        return Container(
          padding: EdgeInsets.all(12.0),
          child: ListView.builder(
            physics: BouncingScrollPhysics(),
            itemCount: snapshot.data.length,
            itemBuilder: (context, index) {
              return ListTile(
                onTap: () {
                  bloc.addMember(snapshot.data[index]);
                  Navigator.of(context).pop();
                },
                title: heading5(text: snapshot.data[index].name),
                subtitle: heading6(text: snapshot.data[index].type),
              );
            },
          ),
        );
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return Container();
  }
}
