import 'dart:io';
import 'package:moor/moor.dart';
import 'package:moor_ffi/moor_ffi.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';
import 'package:toko_ku/service/database/dao/item_dao.dart';
import 'package:toko_ku/service/database/dao/item_unit_dao.dart';
import 'package:toko_ku/service/database/dao/member_dao.dart';
import 'package:toko_ku/service/database/dao/payment_dao.dart';
import 'package:toko_ku/service/database/dao/point_setting_dao.dart';
import 'package:toko_ku/service/database/dao/supplier_dao.dart';
import 'package:toko_ku/service/database/dao/user_transaction_dao.dart';
import 'package:toko_ku/service/database/tables/item_table.dart';
import 'package:toko_ku/service/database/tables/item_unit_table.dart';
import 'package:toko_ku/service/database/tables/member_table.dart';
import 'package:toko_ku/service/database/tables/payment_table.dart';
import 'package:toko_ku/service/database/tables/point_setting_table.dart';
import 'package:toko_ku/service/database/tables/supplier_table.dart';
import 'package:toko_ku/service/database/tables/user_transaction_table.dart';

part 'core_database.g.dart';

/// There is core settings of table and database
/// Lazy open connection database
LazyDatabase _openConnection() {
  return LazyDatabase(() async {
    final dbFolder = await getApplicationDocumentsDirectory();
    final dbFile = File(p.join(dbFolder.path, 'db.sqlite'));
    return VmDatabase(dbFile);
  });
}

@UseMoor(tables: [Members, Suppliers, Items, ItemUnits, UserTransactions, Payments, PointSettings], daos: [SupplierDao, MemberDao, ItemDao, ItemUnitDao, UserTransactionDao, PaymentDao])
class CoreDatabase extends _$CoreDatabase {
  // we tell the database where to store the data with this constructor
  CoreDatabase() : super(_openConnection());

  @override
  int get schemaVersion => 1;
}
