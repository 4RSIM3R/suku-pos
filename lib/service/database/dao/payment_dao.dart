import 'package:moor/moor.dart';
import 'package:toko_ku/service/database/tables/payment_table.dart';

import '../core_database.dart';

part 'payment_dao.g.dart';

@UseDao(tables: [Payments])
class PaymentDao extends DatabaseAccessor<CoreDatabase> with _$PaymentDaoMixin {
  final CoreDatabase db;
  PaymentDao(this.db) : super(db);

  Stream<List<Payment>> get getAllPayment => select(db.payments).watch();
  Future<int> addPayment(Payment entry) => into(db.payments).insert(entry);
  Future<int> deletePayment(Payment entry) => delete(db.payments).delete(entry);
}
