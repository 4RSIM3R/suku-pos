// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'item_unit_dao.dart';

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$ItemUnitDaoMixin on DatabaseAccessor<CoreDatabase> {
  $ItemUnitsTable get itemUnits => db.itemUnits;
}
