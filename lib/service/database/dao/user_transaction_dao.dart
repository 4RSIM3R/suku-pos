import 'package:moor/moor.dart';
import 'package:toko_ku/service/database/tables/user_transaction_table.dart';

import '../core_database.dart';

part 'user_transaction_dao.g.dart';

@UseDao(tables: [UserTransactions])
class UserTransactionDao extends DatabaseAccessor<CoreDatabase> with _$UserTransactionDaoMixin {
  final CoreDatabase db;
  UserTransactionDao(this.db) : super(db);

  Stream<List<UserTransaction>> get getAllUserTransaction => select(db.userTransactions).watch();
  Future<int> addUserTransactions(UserTransaction entry) => into(db.userTransactions).insert(entry);
  Future<int> deleteUserTransaction(UserTransaction entry) => delete(db.userTransactions).delete(entry);

  Stream<List<UserTransaction>> getTransactionByCode(String c) =>
      (select(db.userTransactions)..where((t) => t.code.equals(c))).watch();

  Stream<List<TransactionWithPayment>> getTransactionAndPayment() {
    final query =
        select(db.payments).join([innerJoin(db.userTransactions, db.userTransactions.code.equalsExp(db.payments.code))]);
    return query.watch().map((rows) {
      return rows.map((row) {
        return TransactionWithPayment(
          row.readTable(db.userTransactions),
          row.readTable(db.payments),
        );
      }).toList();
    });
  }

  Future<void> multipleTransactionInsert(List<UserTransaction> list) async {
    await batch((batch) {
      batch.insertAll(db.userTransactions, list);
    });
  }
}

// Some Boilerplate Class Contianing UserTransaction And Payment Class
class TransactionWithPayment {
  final UserTransaction userTransaction;
  final Payment payment;

  TransactionWithPayment(this.userTransaction, this.payment);
}
