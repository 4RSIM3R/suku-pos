// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'item_dao.dart';

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$ItemDaoMixin on DatabaseAccessor<CoreDatabase> {
  $ItemsTable get items => db.items;
}
