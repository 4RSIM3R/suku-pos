import 'package:moor/moor.dart';
import 'package:toko_ku/service/database/tables/item_unit_table.dart';

import '../core_database.dart';

part 'item_unit_dao.g.dart';

@UseDao(tables: [ItemUnits])
class ItemUnitDao extends DatabaseAccessor<CoreDatabase> with _$ItemUnitDaoMixin {
  final CoreDatabase db;
  ItemUnitDao(this.db) : super(db);

  Stream<List<ItemUnit>> get getAllItemUnit => select(db.itemUnits).watch();
  Future<int> addItemUnit(ItemUnit entry) => into(db.itemUnits).insert(entry);
  Future<int> deleteItemUnit(ItemUnit entry) => delete(db.itemUnits).delete(entry);
}
