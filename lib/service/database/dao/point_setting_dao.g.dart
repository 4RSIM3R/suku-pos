// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'point_setting_dao.dart';

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$PointSettingDaoMixin on DatabaseAccessor<CoreDatabase> {
  $PointSettingsTable get pointSettings => db.pointSettings;
}
