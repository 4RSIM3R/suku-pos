import 'package:moor/moor.dart';
import 'package:toko_ku/service/database/tables/point_setting_table.dart';

import '../core_database.dart';

part 'point_setting_dao.g.dart';

@UseDao(tables: [PointSettings])
class PointSettingDao extends DatabaseAccessor<CoreDatabase> with _$PointSettingDaoMixin {
  final CoreDatabase db;
  PointSettingDao(this.db) : super(db);

  /// For operation [CRUD]
  Stream<List<PointSetting>> get getAllPointSettings => select(db.pointSettings).watch();
  Future<int> addPointSetting(PointSetting entry) => into(db.pointSettings).insert(entry);
  Future<void> updatePointSetting(PointSetting entry) => update(db.pointSettings).replace(entry);
  Future<int> deletePointSetting(PointSetting entry) => delete(db.pointSettings).delete(entry);
}
