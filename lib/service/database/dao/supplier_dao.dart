import 'package:moor/moor.dart';

import '../core_database.dart';

part 'supplier_dao.g.dart';

@UseDao(tables: [Supplier])
class SupplierDao extends DatabaseAccessor<CoreDatabase> with _$SupplierDaoMixin {
  final CoreDatabase db;

  // Called by the AppDatabase class
  SupplierDao(this.db) : super(db);
  
  Stream<List<Supplier>> get getAllSuppliers => select(db.suppliers).watch();
  Future<int> addSupplier(Supplier entry) => into(db.suppliers).insert(entry);
  Future<void> updateSupplier(Supplier entry) => update(db.suppliers).replace(entry);
  Future<int> deleteSupplier(Supplier entry) => delete(db.suppliers).delete(entry);
  // Search Supplier
  Stream<List<Supplier>> searchSupplier(String name){
    return (select(db.suppliers)..where((t) => t.name.like('%$name%'))).watch();
  }
}