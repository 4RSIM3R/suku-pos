import 'package:moor/moor.dart';

import '../core_database.dart';

part 'member_dao.g.dart';

@UseDao(tables: [Member])
class MemberDao extends DatabaseAccessor<CoreDatabase> with _$MemberDaoMixin {
  final CoreDatabase db;

  // Called by the AppDatabase class
  MemberDao(this.db) : super(db);

  Stream<List<Member>> get getAllMember => select(db.members).watch();
  Future<int> addMember(Member entry) => into(db.members).insert(entry);
  Future<void> updateMember(Member entry) => update(db.members).replace(entry);
  Future<int> deleteMember(Member entry) => delete(db.members).delete(entry);
  // Search Member
  Stream<List<Member>> searchMember(String name){
    return (select(db.members)..where((t) => t.name.like('%$name%'))).watch();
  }
}