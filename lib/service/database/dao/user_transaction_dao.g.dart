// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_transaction_dao.dart';

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$UserTransactionDaoMixin on DatabaseAccessor<CoreDatabase> {
  $UserTransactionsTable get userTransactions => db.userTransactions;
}
