import 'package:moor/moor.dart';
import 'package:toko_ku/service/database/tables/item_table.dart';

import '../core_database.dart';

part 'item_dao.g.dart';

@UseDao(tables: [Items])
class ItemDao extends DatabaseAccessor<CoreDatabase> with _$ItemDaoMixin {
  final CoreDatabase db;

  // Called by the AppDatabase class
  ItemDao(this.db) : super(db);

  Stream<List<Item>> get getAllItem => select(db.items).watch();
  Future<int> addItem(Item entry) => into(db.items).insert(entry);
  Future<int> deleteItem(Item entry) => delete(db.items).delete(entry);
  Future<void> updateItem(Item entry) => update(db.items).replace(entry);
  // Search Member
  Stream<List<Item>> searchItem(String name) {
    return (select(db.items)..where((t) => t.name.like('%$name%'))).watch();
  }
  // multiple update
  Future<void> massUpdateItem(List<Item> list) async {
    await batch((batch){
      list.forEach((x) => batch.update(items, x));
    });
  }
}
