// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payment_dao.dart';

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$PaymentDaoMixin on DatabaseAccessor<CoreDatabase> {
  $PaymentsTable get payments => db.payments;
}
