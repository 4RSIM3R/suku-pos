// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'core_database.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class Member extends DataClass implements Insertable<Member> {
  final int id;
  final String memberNumber;
  final String name;
  final String address;
  final String identityType;
  final String identityNumber;
  final String phone;
  final String type;
  final int totalPoint;
  Member(
      {@required this.id,
      @required this.memberNumber,
      @required this.name,
      @required this.address,
      @required this.identityType,
      @required this.identityNumber,
      @required this.phone,
      @required this.type,
      @required this.totalPoint});
  factory Member.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    return Member(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      memberNumber: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}member_number']),
      name: stringType.mapFromDatabaseResponse(data['${effectivePrefix}name']),
      address:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}address']),
      identityType: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}identity_type']),
      identityNumber: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}identity_number']),
      phone:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}phone']),
      type: stringType.mapFromDatabaseResponse(data['${effectivePrefix}type']),
      totalPoint: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}total_point']),
    );
  }
  factory Member.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Member(
      id: serializer.fromJson<int>(json['id']),
      memberNumber: serializer.fromJson<String>(json['memberNumber']),
      name: serializer.fromJson<String>(json['name']),
      address: serializer.fromJson<String>(json['address']),
      identityType: serializer.fromJson<String>(json['identityType']),
      identityNumber: serializer.fromJson<String>(json['identityNumber']),
      phone: serializer.fromJson<String>(json['phone']),
      type: serializer.fromJson<String>(json['type']),
      totalPoint: serializer.fromJson<int>(json['totalPoint']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'memberNumber': serializer.toJson<String>(memberNumber),
      'name': serializer.toJson<String>(name),
      'address': serializer.toJson<String>(address),
      'identityType': serializer.toJson<String>(identityType),
      'identityNumber': serializer.toJson<String>(identityNumber),
      'phone': serializer.toJson<String>(phone),
      'type': serializer.toJson<String>(type),
      'totalPoint': serializer.toJson<int>(totalPoint),
    };
  }

  @override
  MembersCompanion createCompanion(bool nullToAbsent) {
    return MembersCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      memberNumber: memberNumber == null && nullToAbsent
          ? const Value.absent()
          : Value(memberNumber),
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
      address: address == null && nullToAbsent
          ? const Value.absent()
          : Value(address),
      identityType: identityType == null && nullToAbsent
          ? const Value.absent()
          : Value(identityType),
      identityNumber: identityNumber == null && nullToAbsent
          ? const Value.absent()
          : Value(identityNumber),
      phone:
          phone == null && nullToAbsent ? const Value.absent() : Value(phone),
      type: type == null && nullToAbsent ? const Value.absent() : Value(type),
      totalPoint: totalPoint == null && nullToAbsent
          ? const Value.absent()
          : Value(totalPoint),
    );
  }

  Member copyWith(
          {int id,
          String memberNumber,
          String name,
          String address,
          String identityType,
          String identityNumber,
          String phone,
          String type,
          int totalPoint}) =>
      Member(
        id: id ?? this.id,
        memberNumber: memberNumber ?? this.memberNumber,
        name: name ?? this.name,
        address: address ?? this.address,
        identityType: identityType ?? this.identityType,
        identityNumber: identityNumber ?? this.identityNumber,
        phone: phone ?? this.phone,
        type: type ?? this.type,
        totalPoint: totalPoint ?? this.totalPoint,
      );
  @override
  String toString() {
    return (StringBuffer('Member(')
          ..write('id: $id, ')
          ..write('memberNumber: $memberNumber, ')
          ..write('name: $name, ')
          ..write('address: $address, ')
          ..write('identityType: $identityType, ')
          ..write('identityNumber: $identityNumber, ')
          ..write('phone: $phone, ')
          ..write('type: $type, ')
          ..write('totalPoint: $totalPoint')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          memberNumber.hashCode,
          $mrjc(
              name.hashCode,
              $mrjc(
                  address.hashCode,
                  $mrjc(
                      identityType.hashCode,
                      $mrjc(
                          identityNumber.hashCode,
                          $mrjc(phone.hashCode,
                              $mrjc(type.hashCode, totalPoint.hashCode)))))))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Member &&
          other.id == this.id &&
          other.memberNumber == this.memberNumber &&
          other.name == this.name &&
          other.address == this.address &&
          other.identityType == this.identityType &&
          other.identityNumber == this.identityNumber &&
          other.phone == this.phone &&
          other.type == this.type &&
          other.totalPoint == this.totalPoint);
}

class MembersCompanion extends UpdateCompanion<Member> {
  final Value<int> id;
  final Value<String> memberNumber;
  final Value<String> name;
  final Value<String> address;
  final Value<String> identityType;
  final Value<String> identityNumber;
  final Value<String> phone;
  final Value<String> type;
  final Value<int> totalPoint;
  const MembersCompanion({
    this.id = const Value.absent(),
    this.memberNumber = const Value.absent(),
    this.name = const Value.absent(),
    this.address = const Value.absent(),
    this.identityType = const Value.absent(),
    this.identityNumber = const Value.absent(),
    this.phone = const Value.absent(),
    this.type = const Value.absent(),
    this.totalPoint = const Value.absent(),
  });
  MembersCompanion.insert({
    this.id = const Value.absent(),
    @required String memberNumber,
    @required String name,
    @required String address,
    @required String identityType,
    @required String identityNumber,
    @required String phone,
    @required String type,
    @required int totalPoint,
  })  : memberNumber = Value(memberNumber),
        name = Value(name),
        address = Value(address),
        identityType = Value(identityType),
        identityNumber = Value(identityNumber),
        phone = Value(phone),
        type = Value(type),
        totalPoint = Value(totalPoint);
  MembersCompanion copyWith(
      {Value<int> id,
      Value<String> memberNumber,
      Value<String> name,
      Value<String> address,
      Value<String> identityType,
      Value<String> identityNumber,
      Value<String> phone,
      Value<String> type,
      Value<int> totalPoint}) {
    return MembersCompanion(
      id: id ?? this.id,
      memberNumber: memberNumber ?? this.memberNumber,
      name: name ?? this.name,
      address: address ?? this.address,
      identityType: identityType ?? this.identityType,
      identityNumber: identityNumber ?? this.identityNumber,
      phone: phone ?? this.phone,
      type: type ?? this.type,
      totalPoint: totalPoint ?? this.totalPoint,
    );
  }
}

class $MembersTable extends Members with TableInfo<$MembersTable, Member> {
  final GeneratedDatabase _db;
  final String _alias;
  $MembersTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _memberNumberMeta =
      const VerificationMeta('memberNumber');
  GeneratedTextColumn _memberNumber;
  @override
  GeneratedTextColumn get memberNumber =>
      _memberNumber ??= _constructMemberNumber();
  GeneratedTextColumn _constructMemberNumber() {
    return GeneratedTextColumn(
      'member_number',
      $tableName,
      false,
    );
  }

  final VerificationMeta _nameMeta = const VerificationMeta('name');
  GeneratedTextColumn _name;
  @override
  GeneratedTextColumn get name => _name ??= _constructName();
  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn(
      'name',
      $tableName,
      false,
    );
  }

  final VerificationMeta _addressMeta = const VerificationMeta('address');
  GeneratedTextColumn _address;
  @override
  GeneratedTextColumn get address => _address ??= _constructAddress();
  GeneratedTextColumn _constructAddress() {
    return GeneratedTextColumn(
      'address',
      $tableName,
      false,
    );
  }

  final VerificationMeta _identityTypeMeta =
      const VerificationMeta('identityType');
  GeneratedTextColumn _identityType;
  @override
  GeneratedTextColumn get identityType =>
      _identityType ??= _constructIdentityType();
  GeneratedTextColumn _constructIdentityType() {
    return GeneratedTextColumn(
      'identity_type',
      $tableName,
      false,
    );
  }

  final VerificationMeta _identityNumberMeta =
      const VerificationMeta('identityNumber');
  GeneratedTextColumn _identityNumber;
  @override
  GeneratedTextColumn get identityNumber =>
      _identityNumber ??= _constructIdentityNumber();
  GeneratedTextColumn _constructIdentityNumber() {
    return GeneratedTextColumn(
      'identity_number',
      $tableName,
      false,
    );
  }

  final VerificationMeta _phoneMeta = const VerificationMeta('phone');
  GeneratedTextColumn _phone;
  @override
  GeneratedTextColumn get phone => _phone ??= _constructPhone();
  GeneratedTextColumn _constructPhone() {
    return GeneratedTextColumn(
      'phone',
      $tableName,
      false,
    );
  }

  final VerificationMeta _typeMeta = const VerificationMeta('type');
  GeneratedTextColumn _type;
  @override
  GeneratedTextColumn get type => _type ??= _constructType();
  GeneratedTextColumn _constructType() {
    return GeneratedTextColumn(
      'type',
      $tableName,
      false,
    );
  }

  final VerificationMeta _totalPointMeta = const VerificationMeta('totalPoint');
  GeneratedIntColumn _totalPoint;
  @override
  GeneratedIntColumn get totalPoint => _totalPoint ??= _constructTotalPoint();
  GeneratedIntColumn _constructTotalPoint() {
    return GeneratedIntColumn(
      'total_point',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [
        id,
        memberNumber,
        name,
        address,
        identityType,
        identityNumber,
        phone,
        type,
        totalPoint
      ];
  @override
  $MembersTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'members';
  @override
  final String actualTableName = 'members';
  @override
  VerificationContext validateIntegrity(MembersCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    }
    if (d.memberNumber.present) {
      context.handle(
          _memberNumberMeta,
          memberNumber.isAcceptableValue(
              d.memberNumber.value, _memberNumberMeta));
    } else if (isInserting) {
      context.missing(_memberNumberMeta);
    }
    if (d.name.present) {
      context.handle(
          _nameMeta, name.isAcceptableValue(d.name.value, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (d.address.present) {
      context.handle(_addressMeta,
          address.isAcceptableValue(d.address.value, _addressMeta));
    } else if (isInserting) {
      context.missing(_addressMeta);
    }
    if (d.identityType.present) {
      context.handle(
          _identityTypeMeta,
          identityType.isAcceptableValue(
              d.identityType.value, _identityTypeMeta));
    } else if (isInserting) {
      context.missing(_identityTypeMeta);
    }
    if (d.identityNumber.present) {
      context.handle(
          _identityNumberMeta,
          identityNumber.isAcceptableValue(
              d.identityNumber.value, _identityNumberMeta));
    } else if (isInserting) {
      context.missing(_identityNumberMeta);
    }
    if (d.phone.present) {
      context.handle(
          _phoneMeta, phone.isAcceptableValue(d.phone.value, _phoneMeta));
    } else if (isInserting) {
      context.missing(_phoneMeta);
    }
    if (d.type.present) {
      context.handle(
          _typeMeta, type.isAcceptableValue(d.type.value, _typeMeta));
    } else if (isInserting) {
      context.missing(_typeMeta);
    }
    if (d.totalPoint.present) {
      context.handle(_totalPointMeta,
          totalPoint.isAcceptableValue(d.totalPoint.value, _totalPointMeta));
    } else if (isInserting) {
      context.missing(_totalPointMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Member map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Member.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(MembersCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.memberNumber.present) {
      map['member_number'] = Variable<String, StringType>(d.memberNumber.value);
    }
    if (d.name.present) {
      map['name'] = Variable<String, StringType>(d.name.value);
    }
    if (d.address.present) {
      map['address'] = Variable<String, StringType>(d.address.value);
    }
    if (d.identityType.present) {
      map['identity_type'] = Variable<String, StringType>(d.identityType.value);
    }
    if (d.identityNumber.present) {
      map['identity_number'] =
          Variable<String, StringType>(d.identityNumber.value);
    }
    if (d.phone.present) {
      map['phone'] = Variable<String, StringType>(d.phone.value);
    }
    if (d.type.present) {
      map['type'] = Variable<String, StringType>(d.type.value);
    }
    if (d.totalPoint.present) {
      map['total_point'] = Variable<int, IntType>(d.totalPoint.value);
    }
    return map;
  }

  @override
  $MembersTable createAlias(String alias) {
    return $MembersTable(_db, alias);
  }
}

class Supplier extends DataClass implements Insertable<Supplier> {
  final int id;
  final String name;
  final String address;
  final String phone;
  Supplier(
      {@required this.id,
      @required this.name,
      @required this.address,
      @required this.phone});
  factory Supplier.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    return Supplier(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      name: stringType.mapFromDatabaseResponse(data['${effectivePrefix}name']),
      address:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}address']),
      phone:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}phone']),
    );
  }
  factory Supplier.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Supplier(
      id: serializer.fromJson<int>(json['id']),
      name: serializer.fromJson<String>(json['name']),
      address: serializer.fromJson<String>(json['address']),
      phone: serializer.fromJson<String>(json['phone']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'name': serializer.toJson<String>(name),
      'address': serializer.toJson<String>(address),
      'phone': serializer.toJson<String>(phone),
    };
  }

  @override
  SuppliersCompanion createCompanion(bool nullToAbsent) {
    return SuppliersCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
      address: address == null && nullToAbsent
          ? const Value.absent()
          : Value(address),
      phone:
          phone == null && nullToAbsent ? const Value.absent() : Value(phone),
    );
  }

  Supplier copyWith({int id, String name, String address, String phone}) =>
      Supplier(
        id: id ?? this.id,
        name: name ?? this.name,
        address: address ?? this.address,
        phone: phone ?? this.phone,
      );
  @override
  String toString() {
    return (StringBuffer('Supplier(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('address: $address, ')
          ..write('phone: $phone')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(id.hashCode,
      $mrjc(name.hashCode, $mrjc(address.hashCode, phone.hashCode))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Supplier &&
          other.id == this.id &&
          other.name == this.name &&
          other.address == this.address &&
          other.phone == this.phone);
}

class SuppliersCompanion extends UpdateCompanion<Supplier> {
  final Value<int> id;
  final Value<String> name;
  final Value<String> address;
  final Value<String> phone;
  const SuppliersCompanion({
    this.id = const Value.absent(),
    this.name = const Value.absent(),
    this.address = const Value.absent(),
    this.phone = const Value.absent(),
  });
  SuppliersCompanion.insert({
    this.id = const Value.absent(),
    @required String name,
    @required String address,
    @required String phone,
  })  : name = Value(name),
        address = Value(address),
        phone = Value(phone);
  SuppliersCompanion copyWith(
      {Value<int> id,
      Value<String> name,
      Value<String> address,
      Value<String> phone}) {
    return SuppliersCompanion(
      id: id ?? this.id,
      name: name ?? this.name,
      address: address ?? this.address,
      phone: phone ?? this.phone,
    );
  }
}

class $SuppliersTable extends Suppliers
    with TableInfo<$SuppliersTable, Supplier> {
  final GeneratedDatabase _db;
  final String _alias;
  $SuppliersTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _nameMeta = const VerificationMeta('name');
  GeneratedTextColumn _name;
  @override
  GeneratedTextColumn get name => _name ??= _constructName();
  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn(
      'name',
      $tableName,
      false,
    );
  }

  final VerificationMeta _addressMeta = const VerificationMeta('address');
  GeneratedTextColumn _address;
  @override
  GeneratedTextColumn get address => _address ??= _constructAddress();
  GeneratedTextColumn _constructAddress() {
    return GeneratedTextColumn(
      'address',
      $tableName,
      false,
    );
  }

  final VerificationMeta _phoneMeta = const VerificationMeta('phone');
  GeneratedTextColumn _phone;
  @override
  GeneratedTextColumn get phone => _phone ??= _constructPhone();
  GeneratedTextColumn _constructPhone() {
    return GeneratedTextColumn(
      'phone',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, name, address, phone];
  @override
  $SuppliersTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'suppliers';
  @override
  final String actualTableName = 'suppliers';
  @override
  VerificationContext validateIntegrity(SuppliersCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    }
    if (d.name.present) {
      context.handle(
          _nameMeta, name.isAcceptableValue(d.name.value, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (d.address.present) {
      context.handle(_addressMeta,
          address.isAcceptableValue(d.address.value, _addressMeta));
    } else if (isInserting) {
      context.missing(_addressMeta);
    }
    if (d.phone.present) {
      context.handle(
          _phoneMeta, phone.isAcceptableValue(d.phone.value, _phoneMeta));
    } else if (isInserting) {
      context.missing(_phoneMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Supplier map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Supplier.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(SuppliersCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.name.present) {
      map['name'] = Variable<String, StringType>(d.name.value);
    }
    if (d.address.present) {
      map['address'] = Variable<String, StringType>(d.address.value);
    }
    if (d.phone.present) {
      map['phone'] = Variable<String, StringType>(d.phone.value);
    }
    return map;
  }

  @override
  $SuppliersTable createAlias(String alias) {
    return $SuppliersTable(_db, alias);
  }
}

class Item extends DataClass implements Insertable<Item> {
  final int id;
  final String name;
  final String barcode;
  final String unit;
  final int priceLevel1;
  final int priceLevel2;
  final int priceLevel3;
  final int minPriceLevel1;
  final int minPriceLevel2;
  final int minPriceLevel3;
  final int discount;
  final int price;
  final int stock;
  Item(
      {@required this.id,
      @required this.name,
      @required this.barcode,
      @required this.unit,
      @required this.priceLevel1,
      @required this.priceLevel2,
      @required this.priceLevel3,
      @required this.minPriceLevel1,
      @required this.minPriceLevel2,
      @required this.minPriceLevel3,
      @required this.discount,
      @required this.price,
      @required this.stock});
  factory Item.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    return Item(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      name: stringType.mapFromDatabaseResponse(data['${effectivePrefix}name']),
      barcode:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}barcode']),
      unit: stringType.mapFromDatabaseResponse(data['${effectivePrefix}unit']),
      priceLevel1: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}price_level1']),
      priceLevel2: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}price_level2']),
      priceLevel3: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}price_level3']),
      minPriceLevel1: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}min_price_level1']),
      minPriceLevel2: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}min_price_level2']),
      minPriceLevel3: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}min_price_level3']),
      discount:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}discount']),
      price: intType.mapFromDatabaseResponse(data['${effectivePrefix}price']),
      stock: intType.mapFromDatabaseResponse(data['${effectivePrefix}stock']),
    );
  }
  factory Item.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Item(
      id: serializer.fromJson<int>(json['id']),
      name: serializer.fromJson<String>(json['name']),
      barcode: serializer.fromJson<String>(json['barcode']),
      unit: serializer.fromJson<String>(json['unit']),
      priceLevel1: serializer.fromJson<int>(json['priceLevel1']),
      priceLevel2: serializer.fromJson<int>(json['priceLevel2']),
      priceLevel3: serializer.fromJson<int>(json['priceLevel3']),
      minPriceLevel1: serializer.fromJson<int>(json['minPriceLevel1']),
      minPriceLevel2: serializer.fromJson<int>(json['minPriceLevel2']),
      minPriceLevel3: serializer.fromJson<int>(json['minPriceLevel3']),
      discount: serializer.fromJson<int>(json['discount']),
      price: serializer.fromJson<int>(json['price']),
      stock: serializer.fromJson<int>(json['stock']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'name': serializer.toJson<String>(name),
      'barcode': serializer.toJson<String>(barcode),
      'unit': serializer.toJson<String>(unit),
      'priceLevel1': serializer.toJson<int>(priceLevel1),
      'priceLevel2': serializer.toJson<int>(priceLevel2),
      'priceLevel3': serializer.toJson<int>(priceLevel3),
      'minPriceLevel1': serializer.toJson<int>(minPriceLevel1),
      'minPriceLevel2': serializer.toJson<int>(minPriceLevel2),
      'minPriceLevel3': serializer.toJson<int>(minPriceLevel3),
      'discount': serializer.toJson<int>(discount),
      'price': serializer.toJson<int>(price),
      'stock': serializer.toJson<int>(stock),
    };
  }

  @override
  ItemsCompanion createCompanion(bool nullToAbsent) {
    return ItemsCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
      barcode: barcode == null && nullToAbsent
          ? const Value.absent()
          : Value(barcode),
      unit: unit == null && nullToAbsent ? const Value.absent() : Value(unit),
      priceLevel1: priceLevel1 == null && nullToAbsent
          ? const Value.absent()
          : Value(priceLevel1),
      priceLevel2: priceLevel2 == null && nullToAbsent
          ? const Value.absent()
          : Value(priceLevel2),
      priceLevel3: priceLevel3 == null && nullToAbsent
          ? const Value.absent()
          : Value(priceLevel3),
      minPriceLevel1: minPriceLevel1 == null && nullToAbsent
          ? const Value.absent()
          : Value(minPriceLevel1),
      minPriceLevel2: minPriceLevel2 == null && nullToAbsent
          ? const Value.absent()
          : Value(minPriceLevel2),
      minPriceLevel3: minPriceLevel3 == null && nullToAbsent
          ? const Value.absent()
          : Value(minPriceLevel3),
      discount: discount == null && nullToAbsent
          ? const Value.absent()
          : Value(discount),
      price:
          price == null && nullToAbsent ? const Value.absent() : Value(price),
      stock:
          stock == null && nullToAbsent ? const Value.absent() : Value(stock),
    );
  }

  Item copyWith(
          {int id,
          String name,
          String barcode,
          String unit,
          int priceLevel1,
          int priceLevel2,
          int priceLevel3,
          int minPriceLevel1,
          int minPriceLevel2,
          int minPriceLevel3,
          int discount,
          int price,
          int stock}) =>
      Item(
        id: id ?? this.id,
        name: name ?? this.name,
        barcode: barcode ?? this.barcode,
        unit: unit ?? this.unit,
        priceLevel1: priceLevel1 ?? this.priceLevel1,
        priceLevel2: priceLevel2 ?? this.priceLevel2,
        priceLevel3: priceLevel3 ?? this.priceLevel3,
        minPriceLevel1: minPriceLevel1 ?? this.minPriceLevel1,
        minPriceLevel2: minPriceLevel2 ?? this.minPriceLevel2,
        minPriceLevel3: minPriceLevel3 ?? this.minPriceLevel3,
        discount: discount ?? this.discount,
        price: price ?? this.price,
        stock: stock ?? this.stock,
      );
  @override
  String toString() {
    return (StringBuffer('Item(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('barcode: $barcode, ')
          ..write('unit: $unit, ')
          ..write('priceLevel1: $priceLevel1, ')
          ..write('priceLevel2: $priceLevel2, ')
          ..write('priceLevel3: $priceLevel3, ')
          ..write('minPriceLevel1: $minPriceLevel1, ')
          ..write('minPriceLevel2: $minPriceLevel2, ')
          ..write('minPriceLevel3: $minPriceLevel3, ')
          ..write('discount: $discount, ')
          ..write('price: $price, ')
          ..write('stock: $stock')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          name.hashCode,
          $mrjc(
              barcode.hashCode,
              $mrjc(
                  unit.hashCode,
                  $mrjc(
                      priceLevel1.hashCode,
                      $mrjc(
                          priceLevel2.hashCode,
                          $mrjc(
                              priceLevel3.hashCode,
                              $mrjc(
                                  minPriceLevel1.hashCode,
                                  $mrjc(
                                      minPriceLevel2.hashCode,
                                      $mrjc(
                                          minPriceLevel3.hashCode,
                                          $mrjc(
                                              discount.hashCode,
                                              $mrjc(price.hashCode,
                                                  stock.hashCode)))))))))))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Item &&
          other.id == this.id &&
          other.name == this.name &&
          other.barcode == this.barcode &&
          other.unit == this.unit &&
          other.priceLevel1 == this.priceLevel1 &&
          other.priceLevel2 == this.priceLevel2 &&
          other.priceLevel3 == this.priceLevel3 &&
          other.minPriceLevel1 == this.minPriceLevel1 &&
          other.minPriceLevel2 == this.minPriceLevel2 &&
          other.minPriceLevel3 == this.minPriceLevel3 &&
          other.discount == this.discount &&
          other.price == this.price &&
          other.stock == this.stock);
}

class ItemsCompanion extends UpdateCompanion<Item> {
  final Value<int> id;
  final Value<String> name;
  final Value<String> barcode;
  final Value<String> unit;
  final Value<int> priceLevel1;
  final Value<int> priceLevel2;
  final Value<int> priceLevel3;
  final Value<int> minPriceLevel1;
  final Value<int> minPriceLevel2;
  final Value<int> minPriceLevel3;
  final Value<int> discount;
  final Value<int> price;
  final Value<int> stock;
  const ItemsCompanion({
    this.id = const Value.absent(),
    this.name = const Value.absent(),
    this.barcode = const Value.absent(),
    this.unit = const Value.absent(),
    this.priceLevel1 = const Value.absent(),
    this.priceLevel2 = const Value.absent(),
    this.priceLevel3 = const Value.absent(),
    this.minPriceLevel1 = const Value.absent(),
    this.minPriceLevel2 = const Value.absent(),
    this.minPriceLevel3 = const Value.absent(),
    this.discount = const Value.absent(),
    this.price = const Value.absent(),
    this.stock = const Value.absent(),
  });
  ItemsCompanion.insert({
    this.id = const Value.absent(),
    @required String name,
    @required String barcode,
    @required String unit,
    @required int priceLevel1,
    @required int priceLevel2,
    @required int priceLevel3,
    @required int minPriceLevel1,
    @required int minPriceLevel2,
    @required int minPriceLevel3,
    @required int discount,
    @required int price,
    @required int stock,
  })  : name = Value(name),
        barcode = Value(barcode),
        unit = Value(unit),
        priceLevel1 = Value(priceLevel1),
        priceLevel2 = Value(priceLevel2),
        priceLevel3 = Value(priceLevel3),
        minPriceLevel1 = Value(minPriceLevel1),
        minPriceLevel2 = Value(minPriceLevel2),
        minPriceLevel3 = Value(minPriceLevel3),
        discount = Value(discount),
        price = Value(price),
        stock = Value(stock);
  ItemsCompanion copyWith(
      {Value<int> id,
      Value<String> name,
      Value<String> barcode,
      Value<String> unit,
      Value<int> priceLevel1,
      Value<int> priceLevel2,
      Value<int> priceLevel3,
      Value<int> minPriceLevel1,
      Value<int> minPriceLevel2,
      Value<int> minPriceLevel3,
      Value<int> discount,
      Value<int> price,
      Value<int> stock}) {
    return ItemsCompanion(
      id: id ?? this.id,
      name: name ?? this.name,
      barcode: barcode ?? this.barcode,
      unit: unit ?? this.unit,
      priceLevel1: priceLevel1 ?? this.priceLevel1,
      priceLevel2: priceLevel2 ?? this.priceLevel2,
      priceLevel3: priceLevel3 ?? this.priceLevel3,
      minPriceLevel1: minPriceLevel1 ?? this.minPriceLevel1,
      minPriceLevel2: minPriceLevel2 ?? this.minPriceLevel2,
      minPriceLevel3: minPriceLevel3 ?? this.minPriceLevel3,
      discount: discount ?? this.discount,
      price: price ?? this.price,
      stock: stock ?? this.stock,
    );
  }
}

class $ItemsTable extends Items with TableInfo<$ItemsTable, Item> {
  final GeneratedDatabase _db;
  final String _alias;
  $ItemsTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _nameMeta = const VerificationMeta('name');
  GeneratedTextColumn _name;
  @override
  GeneratedTextColumn get name => _name ??= _constructName();
  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn(
      'name',
      $tableName,
      false,
    );
  }

  final VerificationMeta _barcodeMeta = const VerificationMeta('barcode');
  GeneratedTextColumn _barcode;
  @override
  GeneratedTextColumn get barcode => _barcode ??= _constructBarcode();
  GeneratedTextColumn _constructBarcode() {
    return GeneratedTextColumn(
      'barcode',
      $tableName,
      false,
    );
  }

  final VerificationMeta _unitMeta = const VerificationMeta('unit');
  GeneratedTextColumn _unit;
  @override
  GeneratedTextColumn get unit => _unit ??= _constructUnit();
  GeneratedTextColumn _constructUnit() {
    return GeneratedTextColumn(
      'unit',
      $tableName,
      false,
    );
  }

  final VerificationMeta _priceLevel1Meta =
      const VerificationMeta('priceLevel1');
  GeneratedIntColumn _priceLevel1;
  @override
  GeneratedIntColumn get priceLevel1 =>
      _priceLevel1 ??= _constructPriceLevel1();
  GeneratedIntColumn _constructPriceLevel1() {
    return GeneratedIntColumn(
      'price_level1',
      $tableName,
      false,
    );
  }

  final VerificationMeta _priceLevel2Meta =
      const VerificationMeta('priceLevel2');
  GeneratedIntColumn _priceLevel2;
  @override
  GeneratedIntColumn get priceLevel2 =>
      _priceLevel2 ??= _constructPriceLevel2();
  GeneratedIntColumn _constructPriceLevel2() {
    return GeneratedIntColumn(
      'price_level2',
      $tableName,
      false,
    );
  }

  final VerificationMeta _priceLevel3Meta =
      const VerificationMeta('priceLevel3');
  GeneratedIntColumn _priceLevel3;
  @override
  GeneratedIntColumn get priceLevel3 =>
      _priceLevel3 ??= _constructPriceLevel3();
  GeneratedIntColumn _constructPriceLevel3() {
    return GeneratedIntColumn(
      'price_level3',
      $tableName,
      false,
    );
  }

  final VerificationMeta _minPriceLevel1Meta =
      const VerificationMeta('minPriceLevel1');
  GeneratedIntColumn _minPriceLevel1;
  @override
  GeneratedIntColumn get minPriceLevel1 =>
      _minPriceLevel1 ??= _constructMinPriceLevel1();
  GeneratedIntColumn _constructMinPriceLevel1() {
    return GeneratedIntColumn(
      'min_price_level1',
      $tableName,
      false,
    );
  }

  final VerificationMeta _minPriceLevel2Meta =
      const VerificationMeta('minPriceLevel2');
  GeneratedIntColumn _minPriceLevel2;
  @override
  GeneratedIntColumn get minPriceLevel2 =>
      _minPriceLevel2 ??= _constructMinPriceLevel2();
  GeneratedIntColumn _constructMinPriceLevel2() {
    return GeneratedIntColumn(
      'min_price_level2',
      $tableName,
      false,
    );
  }

  final VerificationMeta _minPriceLevel3Meta =
      const VerificationMeta('minPriceLevel3');
  GeneratedIntColumn _minPriceLevel3;
  @override
  GeneratedIntColumn get minPriceLevel3 =>
      _minPriceLevel3 ??= _constructMinPriceLevel3();
  GeneratedIntColumn _constructMinPriceLevel3() {
    return GeneratedIntColumn(
      'min_price_level3',
      $tableName,
      false,
    );
  }

  final VerificationMeta _discountMeta = const VerificationMeta('discount');
  GeneratedIntColumn _discount;
  @override
  GeneratedIntColumn get discount => _discount ??= _constructDiscount();
  GeneratedIntColumn _constructDiscount() {
    return GeneratedIntColumn(
      'discount',
      $tableName,
      false,
    );
  }

  final VerificationMeta _priceMeta = const VerificationMeta('price');
  GeneratedIntColumn _price;
  @override
  GeneratedIntColumn get price => _price ??= _constructPrice();
  GeneratedIntColumn _constructPrice() {
    return GeneratedIntColumn(
      'price',
      $tableName,
      false,
    );
  }

  final VerificationMeta _stockMeta = const VerificationMeta('stock');
  GeneratedIntColumn _stock;
  @override
  GeneratedIntColumn get stock => _stock ??= _constructStock();
  GeneratedIntColumn _constructStock() {
    return GeneratedIntColumn(
      'stock',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [
        id,
        name,
        barcode,
        unit,
        priceLevel1,
        priceLevel2,
        priceLevel3,
        minPriceLevel1,
        minPriceLevel2,
        minPriceLevel3,
        discount,
        price,
        stock
      ];
  @override
  $ItemsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'items';
  @override
  final String actualTableName = 'items';
  @override
  VerificationContext validateIntegrity(ItemsCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    }
    if (d.name.present) {
      context.handle(
          _nameMeta, name.isAcceptableValue(d.name.value, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (d.barcode.present) {
      context.handle(_barcodeMeta,
          barcode.isAcceptableValue(d.barcode.value, _barcodeMeta));
    } else if (isInserting) {
      context.missing(_barcodeMeta);
    }
    if (d.unit.present) {
      context.handle(
          _unitMeta, unit.isAcceptableValue(d.unit.value, _unitMeta));
    } else if (isInserting) {
      context.missing(_unitMeta);
    }
    if (d.priceLevel1.present) {
      context.handle(_priceLevel1Meta,
          priceLevel1.isAcceptableValue(d.priceLevel1.value, _priceLevel1Meta));
    } else if (isInserting) {
      context.missing(_priceLevel1Meta);
    }
    if (d.priceLevel2.present) {
      context.handle(_priceLevel2Meta,
          priceLevel2.isAcceptableValue(d.priceLevel2.value, _priceLevel2Meta));
    } else if (isInserting) {
      context.missing(_priceLevel2Meta);
    }
    if (d.priceLevel3.present) {
      context.handle(_priceLevel3Meta,
          priceLevel3.isAcceptableValue(d.priceLevel3.value, _priceLevel3Meta));
    } else if (isInserting) {
      context.missing(_priceLevel3Meta);
    }
    if (d.minPriceLevel1.present) {
      context.handle(
          _minPriceLevel1Meta,
          minPriceLevel1.isAcceptableValue(
              d.minPriceLevel1.value, _minPriceLevel1Meta));
    } else if (isInserting) {
      context.missing(_minPriceLevel1Meta);
    }
    if (d.minPriceLevel2.present) {
      context.handle(
          _minPriceLevel2Meta,
          minPriceLevel2.isAcceptableValue(
              d.minPriceLevel2.value, _minPriceLevel2Meta));
    } else if (isInserting) {
      context.missing(_minPriceLevel2Meta);
    }
    if (d.minPriceLevel3.present) {
      context.handle(
          _minPriceLevel3Meta,
          minPriceLevel3.isAcceptableValue(
              d.minPriceLevel3.value, _minPriceLevel3Meta));
    } else if (isInserting) {
      context.missing(_minPriceLevel3Meta);
    }
    if (d.discount.present) {
      context.handle(_discountMeta,
          discount.isAcceptableValue(d.discount.value, _discountMeta));
    } else if (isInserting) {
      context.missing(_discountMeta);
    }
    if (d.price.present) {
      context.handle(
          _priceMeta, price.isAcceptableValue(d.price.value, _priceMeta));
    } else if (isInserting) {
      context.missing(_priceMeta);
    }
    if (d.stock.present) {
      context.handle(
          _stockMeta, stock.isAcceptableValue(d.stock.value, _stockMeta));
    } else if (isInserting) {
      context.missing(_stockMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Item map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Item.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(ItemsCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.name.present) {
      map['name'] = Variable<String, StringType>(d.name.value);
    }
    if (d.barcode.present) {
      map['barcode'] = Variable<String, StringType>(d.barcode.value);
    }
    if (d.unit.present) {
      map['unit'] = Variable<String, StringType>(d.unit.value);
    }
    if (d.priceLevel1.present) {
      map['price_level1'] = Variable<int, IntType>(d.priceLevel1.value);
    }
    if (d.priceLevel2.present) {
      map['price_level2'] = Variable<int, IntType>(d.priceLevel2.value);
    }
    if (d.priceLevel3.present) {
      map['price_level3'] = Variable<int, IntType>(d.priceLevel3.value);
    }
    if (d.minPriceLevel1.present) {
      map['min_price_level1'] = Variable<int, IntType>(d.minPriceLevel1.value);
    }
    if (d.minPriceLevel2.present) {
      map['min_price_level2'] = Variable<int, IntType>(d.minPriceLevel2.value);
    }
    if (d.minPriceLevel3.present) {
      map['min_price_level3'] = Variable<int, IntType>(d.minPriceLevel3.value);
    }
    if (d.discount.present) {
      map['discount'] = Variable<int, IntType>(d.discount.value);
    }
    if (d.price.present) {
      map['price'] = Variable<int, IntType>(d.price.value);
    }
    if (d.stock.present) {
      map['stock'] = Variable<int, IntType>(d.stock.value);
    }
    return map;
  }

  @override
  $ItemsTable createAlias(String alias) {
    return $ItemsTable(_db, alias);
  }
}

class ItemUnit extends DataClass implements Insertable<ItemUnit> {
  final int id;
  final String name;
  ItemUnit({@required this.id, @required this.name});
  factory ItemUnit.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    return ItemUnit(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      name: stringType.mapFromDatabaseResponse(data['${effectivePrefix}name']),
    );
  }
  factory ItemUnit.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return ItemUnit(
      id: serializer.fromJson<int>(json['id']),
      name: serializer.fromJson<String>(json['name']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'name': serializer.toJson<String>(name),
    };
  }

  @override
  ItemUnitsCompanion createCompanion(bool nullToAbsent) {
    return ItemUnitsCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
    );
  }

  ItemUnit copyWith({int id, String name}) => ItemUnit(
        id: id ?? this.id,
        name: name ?? this.name,
      );
  @override
  String toString() {
    return (StringBuffer('ItemUnit(')
          ..write('id: $id, ')
          ..write('name: $name')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(id.hashCode, name.hashCode));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is ItemUnit && other.id == this.id && other.name == this.name);
}

class ItemUnitsCompanion extends UpdateCompanion<ItemUnit> {
  final Value<int> id;
  final Value<String> name;
  const ItemUnitsCompanion({
    this.id = const Value.absent(),
    this.name = const Value.absent(),
  });
  ItemUnitsCompanion.insert({
    this.id = const Value.absent(),
    @required String name,
  }) : name = Value(name);
  ItemUnitsCompanion copyWith({Value<int> id, Value<String> name}) {
    return ItemUnitsCompanion(
      id: id ?? this.id,
      name: name ?? this.name,
    );
  }
}

class $ItemUnitsTable extends ItemUnits
    with TableInfo<$ItemUnitsTable, ItemUnit> {
  final GeneratedDatabase _db;
  final String _alias;
  $ItemUnitsTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _nameMeta = const VerificationMeta('name');
  GeneratedTextColumn _name;
  @override
  GeneratedTextColumn get name => _name ??= _constructName();
  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn(
      'name',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, name];
  @override
  $ItemUnitsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'item_units';
  @override
  final String actualTableName = 'item_units';
  @override
  VerificationContext validateIntegrity(ItemUnitsCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    }
    if (d.name.present) {
      context.handle(
          _nameMeta, name.isAcceptableValue(d.name.value, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  ItemUnit map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return ItemUnit.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(ItemUnitsCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.name.present) {
      map['name'] = Variable<String, StringType>(d.name.value);
    }
    return map;
  }

  @override
  $ItemUnitsTable createAlias(String alias) {
    return $ItemUnitsTable(_db, alias);
  }
}

class UserTransaction extends DataClass implements Insertable<UserTransaction> {
  final int id;
  final String code;
  final String name;
  final int quantity;
  final int unitPrice;
  final int totalUnitPrice;
  final DateTime date;
  UserTransaction(
      {@required this.id,
      @required this.code,
      @required this.name,
      @required this.quantity,
      @required this.unitPrice,
      @required this.totalUnitPrice,
      @required this.date});
  factory UserTransaction.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return UserTransaction(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      code: stringType.mapFromDatabaseResponse(data['${effectivePrefix}code']),
      name: stringType.mapFromDatabaseResponse(data['${effectivePrefix}name']),
      quantity:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}quantity']),
      unitPrice:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}unit_price']),
      totalUnitPrice: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}total_unit_price']),
      date:
          dateTimeType.mapFromDatabaseResponse(data['${effectivePrefix}date']),
    );
  }
  factory UserTransaction.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return UserTransaction(
      id: serializer.fromJson<int>(json['id']),
      code: serializer.fromJson<String>(json['code']),
      name: serializer.fromJson<String>(json['name']),
      quantity: serializer.fromJson<int>(json['quantity']),
      unitPrice: serializer.fromJson<int>(json['unitPrice']),
      totalUnitPrice: serializer.fromJson<int>(json['totalUnitPrice']),
      date: serializer.fromJson<DateTime>(json['date']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'code': serializer.toJson<String>(code),
      'name': serializer.toJson<String>(name),
      'quantity': serializer.toJson<int>(quantity),
      'unitPrice': serializer.toJson<int>(unitPrice),
      'totalUnitPrice': serializer.toJson<int>(totalUnitPrice),
      'date': serializer.toJson<DateTime>(date),
    };
  }

  @override
  UserTransactionsCompanion createCompanion(bool nullToAbsent) {
    return UserTransactionsCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      code: code == null && nullToAbsent ? const Value.absent() : Value(code),
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
      quantity: quantity == null && nullToAbsent
          ? const Value.absent()
          : Value(quantity),
      unitPrice: unitPrice == null && nullToAbsent
          ? const Value.absent()
          : Value(unitPrice),
      totalUnitPrice: totalUnitPrice == null && nullToAbsent
          ? const Value.absent()
          : Value(totalUnitPrice),
      date: date == null && nullToAbsent ? const Value.absent() : Value(date),
    );
  }

  UserTransaction copyWith(
          {int id,
          String code,
          String name,
          int quantity,
          int unitPrice,
          int totalUnitPrice,
          DateTime date}) =>
      UserTransaction(
        id: id ?? this.id,
        code: code ?? this.code,
        name: name ?? this.name,
        quantity: quantity ?? this.quantity,
        unitPrice: unitPrice ?? this.unitPrice,
        totalUnitPrice: totalUnitPrice ?? this.totalUnitPrice,
        date: date ?? this.date,
      );
  @override
  String toString() {
    return (StringBuffer('UserTransaction(')
          ..write('id: $id, ')
          ..write('code: $code, ')
          ..write('name: $name, ')
          ..write('quantity: $quantity, ')
          ..write('unitPrice: $unitPrice, ')
          ..write('totalUnitPrice: $totalUnitPrice, ')
          ..write('date: $date')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          code.hashCode,
          $mrjc(
              name.hashCode,
              $mrjc(
                  quantity.hashCode,
                  $mrjc(unitPrice.hashCode,
                      $mrjc(totalUnitPrice.hashCode, date.hashCode)))))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is UserTransaction &&
          other.id == this.id &&
          other.code == this.code &&
          other.name == this.name &&
          other.quantity == this.quantity &&
          other.unitPrice == this.unitPrice &&
          other.totalUnitPrice == this.totalUnitPrice &&
          other.date == this.date);
}

class UserTransactionsCompanion extends UpdateCompanion<UserTransaction> {
  final Value<int> id;
  final Value<String> code;
  final Value<String> name;
  final Value<int> quantity;
  final Value<int> unitPrice;
  final Value<int> totalUnitPrice;
  final Value<DateTime> date;
  const UserTransactionsCompanion({
    this.id = const Value.absent(),
    this.code = const Value.absent(),
    this.name = const Value.absent(),
    this.quantity = const Value.absent(),
    this.unitPrice = const Value.absent(),
    this.totalUnitPrice = const Value.absent(),
    this.date = const Value.absent(),
  });
  UserTransactionsCompanion.insert({
    this.id = const Value.absent(),
    @required String code,
    @required String name,
    @required int quantity,
    @required int unitPrice,
    @required int totalUnitPrice,
    @required DateTime date,
  })  : code = Value(code),
        name = Value(name),
        quantity = Value(quantity),
        unitPrice = Value(unitPrice),
        totalUnitPrice = Value(totalUnitPrice),
        date = Value(date);
  UserTransactionsCompanion copyWith(
      {Value<int> id,
      Value<String> code,
      Value<String> name,
      Value<int> quantity,
      Value<int> unitPrice,
      Value<int> totalUnitPrice,
      Value<DateTime> date}) {
    return UserTransactionsCompanion(
      id: id ?? this.id,
      code: code ?? this.code,
      name: name ?? this.name,
      quantity: quantity ?? this.quantity,
      unitPrice: unitPrice ?? this.unitPrice,
      totalUnitPrice: totalUnitPrice ?? this.totalUnitPrice,
      date: date ?? this.date,
    );
  }
}

class $UserTransactionsTable extends UserTransactions
    with TableInfo<$UserTransactionsTable, UserTransaction> {
  final GeneratedDatabase _db;
  final String _alias;
  $UserTransactionsTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _codeMeta = const VerificationMeta('code');
  GeneratedTextColumn _code;
  @override
  GeneratedTextColumn get code => _code ??= _constructCode();
  GeneratedTextColumn _constructCode() {
    return GeneratedTextColumn(
      'code',
      $tableName,
      false,
    );
  }

  final VerificationMeta _nameMeta = const VerificationMeta('name');
  GeneratedTextColumn _name;
  @override
  GeneratedTextColumn get name => _name ??= _constructName();
  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn(
      'name',
      $tableName,
      false,
    );
  }

  final VerificationMeta _quantityMeta = const VerificationMeta('quantity');
  GeneratedIntColumn _quantity;
  @override
  GeneratedIntColumn get quantity => _quantity ??= _constructQuantity();
  GeneratedIntColumn _constructQuantity() {
    return GeneratedIntColumn(
      'quantity',
      $tableName,
      false,
    );
  }

  final VerificationMeta _unitPriceMeta = const VerificationMeta('unitPrice');
  GeneratedIntColumn _unitPrice;
  @override
  GeneratedIntColumn get unitPrice => _unitPrice ??= _constructUnitPrice();
  GeneratedIntColumn _constructUnitPrice() {
    return GeneratedIntColumn(
      'unit_price',
      $tableName,
      false,
    );
  }

  final VerificationMeta _totalUnitPriceMeta =
      const VerificationMeta('totalUnitPrice');
  GeneratedIntColumn _totalUnitPrice;
  @override
  GeneratedIntColumn get totalUnitPrice =>
      _totalUnitPrice ??= _constructTotalUnitPrice();
  GeneratedIntColumn _constructTotalUnitPrice() {
    return GeneratedIntColumn(
      'total_unit_price',
      $tableName,
      false,
    );
  }

  final VerificationMeta _dateMeta = const VerificationMeta('date');
  GeneratedDateTimeColumn _date;
  @override
  GeneratedDateTimeColumn get date => _date ??= _constructDate();
  GeneratedDateTimeColumn _constructDate() {
    return GeneratedDateTimeColumn(
      'date',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns =>
      [id, code, name, quantity, unitPrice, totalUnitPrice, date];
  @override
  $UserTransactionsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'user_transactions';
  @override
  final String actualTableName = 'user_transactions';
  @override
  VerificationContext validateIntegrity(UserTransactionsCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    }
    if (d.code.present) {
      context.handle(
          _codeMeta, code.isAcceptableValue(d.code.value, _codeMeta));
    } else if (isInserting) {
      context.missing(_codeMeta);
    }
    if (d.name.present) {
      context.handle(
          _nameMeta, name.isAcceptableValue(d.name.value, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (d.quantity.present) {
      context.handle(_quantityMeta,
          quantity.isAcceptableValue(d.quantity.value, _quantityMeta));
    } else if (isInserting) {
      context.missing(_quantityMeta);
    }
    if (d.unitPrice.present) {
      context.handle(_unitPriceMeta,
          unitPrice.isAcceptableValue(d.unitPrice.value, _unitPriceMeta));
    } else if (isInserting) {
      context.missing(_unitPriceMeta);
    }
    if (d.totalUnitPrice.present) {
      context.handle(
          _totalUnitPriceMeta,
          totalUnitPrice.isAcceptableValue(
              d.totalUnitPrice.value, _totalUnitPriceMeta));
    } else if (isInserting) {
      context.missing(_totalUnitPriceMeta);
    }
    if (d.date.present) {
      context.handle(
          _dateMeta, date.isAcceptableValue(d.date.value, _dateMeta));
    } else if (isInserting) {
      context.missing(_dateMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  UserTransaction map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return UserTransaction.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(UserTransactionsCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.code.present) {
      map['code'] = Variable<String, StringType>(d.code.value);
    }
    if (d.name.present) {
      map['name'] = Variable<String, StringType>(d.name.value);
    }
    if (d.quantity.present) {
      map['quantity'] = Variable<int, IntType>(d.quantity.value);
    }
    if (d.unitPrice.present) {
      map['unit_price'] = Variable<int, IntType>(d.unitPrice.value);
    }
    if (d.totalUnitPrice.present) {
      map['total_unit_price'] = Variable<int, IntType>(d.totalUnitPrice.value);
    }
    if (d.date.present) {
      map['date'] = Variable<DateTime, DateTimeType>(d.date.value);
    }
    return map;
  }

  @override
  $UserTransactionsTable createAlias(String alias) {
    return $UserTransactionsTable(_db, alias);
  }
}

class Payment extends DataClass implements Insertable<Payment> {
  final int id;
  final String code;
  final int totalPrice;
  final int buyerMoney;
  final int buyerCharge;
  final DateTime date;
  Payment(
      {@required this.id,
      @required this.code,
      @required this.totalPrice,
      @required this.buyerMoney,
      @required this.buyerCharge,
      @required this.date});
  factory Payment.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return Payment(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      code: stringType.mapFromDatabaseResponse(data['${effectivePrefix}code']),
      totalPrice: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}total_price']),
      buyerMoney: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}buyer_money']),
      buyerCharge: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}buyer_charge']),
      date:
          dateTimeType.mapFromDatabaseResponse(data['${effectivePrefix}date']),
    );
  }
  factory Payment.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Payment(
      id: serializer.fromJson<int>(json['id']),
      code: serializer.fromJson<String>(json['code']),
      totalPrice: serializer.fromJson<int>(json['totalPrice']),
      buyerMoney: serializer.fromJson<int>(json['buyerMoney']),
      buyerCharge: serializer.fromJson<int>(json['buyerCharge']),
      date: serializer.fromJson<DateTime>(json['date']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'code': serializer.toJson<String>(code),
      'totalPrice': serializer.toJson<int>(totalPrice),
      'buyerMoney': serializer.toJson<int>(buyerMoney),
      'buyerCharge': serializer.toJson<int>(buyerCharge),
      'date': serializer.toJson<DateTime>(date),
    };
  }

  @override
  PaymentsCompanion createCompanion(bool nullToAbsent) {
    return PaymentsCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      code: code == null && nullToAbsent ? const Value.absent() : Value(code),
      totalPrice: totalPrice == null && nullToAbsent
          ? const Value.absent()
          : Value(totalPrice),
      buyerMoney: buyerMoney == null && nullToAbsent
          ? const Value.absent()
          : Value(buyerMoney),
      buyerCharge: buyerCharge == null && nullToAbsent
          ? const Value.absent()
          : Value(buyerCharge),
      date: date == null && nullToAbsent ? const Value.absent() : Value(date),
    );
  }

  Payment copyWith(
          {int id,
          String code,
          int totalPrice,
          int buyerMoney,
          int buyerCharge,
          DateTime date}) =>
      Payment(
        id: id ?? this.id,
        code: code ?? this.code,
        totalPrice: totalPrice ?? this.totalPrice,
        buyerMoney: buyerMoney ?? this.buyerMoney,
        buyerCharge: buyerCharge ?? this.buyerCharge,
        date: date ?? this.date,
      );
  @override
  String toString() {
    return (StringBuffer('Payment(')
          ..write('id: $id, ')
          ..write('code: $code, ')
          ..write('totalPrice: $totalPrice, ')
          ..write('buyerMoney: $buyerMoney, ')
          ..write('buyerCharge: $buyerCharge, ')
          ..write('date: $date')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          code.hashCode,
          $mrjc(
              totalPrice.hashCode,
              $mrjc(buyerMoney.hashCode,
                  $mrjc(buyerCharge.hashCode, date.hashCode))))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Payment &&
          other.id == this.id &&
          other.code == this.code &&
          other.totalPrice == this.totalPrice &&
          other.buyerMoney == this.buyerMoney &&
          other.buyerCharge == this.buyerCharge &&
          other.date == this.date);
}

class PaymentsCompanion extends UpdateCompanion<Payment> {
  final Value<int> id;
  final Value<String> code;
  final Value<int> totalPrice;
  final Value<int> buyerMoney;
  final Value<int> buyerCharge;
  final Value<DateTime> date;
  const PaymentsCompanion({
    this.id = const Value.absent(),
    this.code = const Value.absent(),
    this.totalPrice = const Value.absent(),
    this.buyerMoney = const Value.absent(),
    this.buyerCharge = const Value.absent(),
    this.date = const Value.absent(),
  });
  PaymentsCompanion.insert({
    this.id = const Value.absent(),
    @required String code,
    @required int totalPrice,
    @required int buyerMoney,
    @required int buyerCharge,
    @required DateTime date,
  })  : code = Value(code),
        totalPrice = Value(totalPrice),
        buyerMoney = Value(buyerMoney),
        buyerCharge = Value(buyerCharge),
        date = Value(date);
  PaymentsCompanion copyWith(
      {Value<int> id,
      Value<String> code,
      Value<int> totalPrice,
      Value<int> buyerMoney,
      Value<int> buyerCharge,
      Value<DateTime> date}) {
    return PaymentsCompanion(
      id: id ?? this.id,
      code: code ?? this.code,
      totalPrice: totalPrice ?? this.totalPrice,
      buyerMoney: buyerMoney ?? this.buyerMoney,
      buyerCharge: buyerCharge ?? this.buyerCharge,
      date: date ?? this.date,
    );
  }
}

class $PaymentsTable extends Payments with TableInfo<$PaymentsTable, Payment> {
  final GeneratedDatabase _db;
  final String _alias;
  $PaymentsTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _codeMeta = const VerificationMeta('code');
  GeneratedTextColumn _code;
  @override
  GeneratedTextColumn get code => _code ??= _constructCode();
  GeneratedTextColumn _constructCode() {
    return GeneratedTextColumn(
      'code',
      $tableName,
      false,
    );
  }

  final VerificationMeta _totalPriceMeta = const VerificationMeta('totalPrice');
  GeneratedIntColumn _totalPrice;
  @override
  GeneratedIntColumn get totalPrice => _totalPrice ??= _constructTotalPrice();
  GeneratedIntColumn _constructTotalPrice() {
    return GeneratedIntColumn(
      'total_price',
      $tableName,
      false,
    );
  }

  final VerificationMeta _buyerMoneyMeta = const VerificationMeta('buyerMoney');
  GeneratedIntColumn _buyerMoney;
  @override
  GeneratedIntColumn get buyerMoney => _buyerMoney ??= _constructBuyerMoney();
  GeneratedIntColumn _constructBuyerMoney() {
    return GeneratedIntColumn(
      'buyer_money',
      $tableName,
      false,
    );
  }

  final VerificationMeta _buyerChargeMeta =
      const VerificationMeta('buyerCharge');
  GeneratedIntColumn _buyerCharge;
  @override
  GeneratedIntColumn get buyerCharge =>
      _buyerCharge ??= _constructBuyerCharge();
  GeneratedIntColumn _constructBuyerCharge() {
    return GeneratedIntColumn(
      'buyer_charge',
      $tableName,
      false,
    );
  }

  final VerificationMeta _dateMeta = const VerificationMeta('date');
  GeneratedDateTimeColumn _date;
  @override
  GeneratedDateTimeColumn get date => _date ??= _constructDate();
  GeneratedDateTimeColumn _constructDate() {
    return GeneratedDateTimeColumn(
      'date',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns =>
      [id, code, totalPrice, buyerMoney, buyerCharge, date];
  @override
  $PaymentsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'payments';
  @override
  final String actualTableName = 'payments';
  @override
  VerificationContext validateIntegrity(PaymentsCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    }
    if (d.code.present) {
      context.handle(
          _codeMeta, code.isAcceptableValue(d.code.value, _codeMeta));
    } else if (isInserting) {
      context.missing(_codeMeta);
    }
    if (d.totalPrice.present) {
      context.handle(_totalPriceMeta,
          totalPrice.isAcceptableValue(d.totalPrice.value, _totalPriceMeta));
    } else if (isInserting) {
      context.missing(_totalPriceMeta);
    }
    if (d.buyerMoney.present) {
      context.handle(_buyerMoneyMeta,
          buyerMoney.isAcceptableValue(d.buyerMoney.value, _buyerMoneyMeta));
    } else if (isInserting) {
      context.missing(_buyerMoneyMeta);
    }
    if (d.buyerCharge.present) {
      context.handle(_buyerChargeMeta,
          buyerCharge.isAcceptableValue(d.buyerCharge.value, _buyerChargeMeta));
    } else if (isInserting) {
      context.missing(_buyerChargeMeta);
    }
    if (d.date.present) {
      context.handle(
          _dateMeta, date.isAcceptableValue(d.date.value, _dateMeta));
    } else if (isInserting) {
      context.missing(_dateMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Payment map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Payment.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(PaymentsCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.code.present) {
      map['code'] = Variable<String, StringType>(d.code.value);
    }
    if (d.totalPrice.present) {
      map['total_price'] = Variable<int, IntType>(d.totalPrice.value);
    }
    if (d.buyerMoney.present) {
      map['buyer_money'] = Variable<int, IntType>(d.buyerMoney.value);
    }
    if (d.buyerCharge.present) {
      map['buyer_charge'] = Variable<int, IntType>(d.buyerCharge.value);
    }
    if (d.date.present) {
      map['date'] = Variable<DateTime, DateTimeType>(d.date.value);
    }
    return map;
  }

  @override
  $PaymentsTable createAlias(String alias) {
    return $PaymentsTable(_db, alias);
  }
}

class PointSetting extends DataClass implements Insertable<PointSetting> {
  final int id;
  final int minTransaction;
  final int pointAwarded;
  PointSetting(
      {@required this.id,
      @required this.minTransaction,
      @required this.pointAwarded});
  factory PointSetting.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    return PointSetting(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      minTransaction: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}min_transaction']),
      pointAwarded: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}point_awarded']),
    );
  }
  factory PointSetting.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return PointSetting(
      id: serializer.fromJson<int>(json['id']),
      minTransaction: serializer.fromJson<int>(json['minTransaction']),
      pointAwarded: serializer.fromJson<int>(json['pointAwarded']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'minTransaction': serializer.toJson<int>(minTransaction),
      'pointAwarded': serializer.toJson<int>(pointAwarded),
    };
  }

  @override
  PointSettingsCompanion createCompanion(bool nullToAbsent) {
    return PointSettingsCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      minTransaction: minTransaction == null && nullToAbsent
          ? const Value.absent()
          : Value(minTransaction),
      pointAwarded: pointAwarded == null && nullToAbsent
          ? const Value.absent()
          : Value(pointAwarded),
    );
  }

  PointSetting copyWith({int id, int minTransaction, int pointAwarded}) =>
      PointSetting(
        id: id ?? this.id,
        minTransaction: minTransaction ?? this.minTransaction,
        pointAwarded: pointAwarded ?? this.pointAwarded,
      );
  @override
  String toString() {
    return (StringBuffer('PointSetting(')
          ..write('id: $id, ')
          ..write('minTransaction: $minTransaction, ')
          ..write('pointAwarded: $pointAwarded')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode, $mrjc(minTransaction.hashCode, pointAwarded.hashCode)));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is PointSetting &&
          other.id == this.id &&
          other.minTransaction == this.minTransaction &&
          other.pointAwarded == this.pointAwarded);
}

class PointSettingsCompanion extends UpdateCompanion<PointSetting> {
  final Value<int> id;
  final Value<int> minTransaction;
  final Value<int> pointAwarded;
  const PointSettingsCompanion({
    this.id = const Value.absent(),
    this.minTransaction = const Value.absent(),
    this.pointAwarded = const Value.absent(),
  });
  PointSettingsCompanion.insert({
    this.id = const Value.absent(),
    @required int minTransaction,
    @required int pointAwarded,
  })  : minTransaction = Value(minTransaction),
        pointAwarded = Value(pointAwarded);
  PointSettingsCompanion copyWith(
      {Value<int> id, Value<int> minTransaction, Value<int> pointAwarded}) {
    return PointSettingsCompanion(
      id: id ?? this.id,
      minTransaction: minTransaction ?? this.minTransaction,
      pointAwarded: pointAwarded ?? this.pointAwarded,
    );
  }
}

class $PointSettingsTable extends PointSettings
    with TableInfo<$PointSettingsTable, PointSetting> {
  final GeneratedDatabase _db;
  final String _alias;
  $PointSettingsTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _minTransactionMeta =
      const VerificationMeta('minTransaction');
  GeneratedIntColumn _minTransaction;
  @override
  GeneratedIntColumn get minTransaction =>
      _minTransaction ??= _constructMinTransaction();
  GeneratedIntColumn _constructMinTransaction() {
    return GeneratedIntColumn(
      'min_transaction',
      $tableName,
      false,
    );
  }

  final VerificationMeta _pointAwardedMeta =
      const VerificationMeta('pointAwarded');
  GeneratedIntColumn _pointAwarded;
  @override
  GeneratedIntColumn get pointAwarded =>
      _pointAwarded ??= _constructPointAwarded();
  GeneratedIntColumn _constructPointAwarded() {
    return GeneratedIntColumn(
      'point_awarded',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, minTransaction, pointAwarded];
  @override
  $PointSettingsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'point_settings';
  @override
  final String actualTableName = 'point_settings';
  @override
  VerificationContext validateIntegrity(PointSettingsCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    }
    if (d.minTransaction.present) {
      context.handle(
          _minTransactionMeta,
          minTransaction.isAcceptableValue(
              d.minTransaction.value, _minTransactionMeta));
    } else if (isInserting) {
      context.missing(_minTransactionMeta);
    }
    if (d.pointAwarded.present) {
      context.handle(
          _pointAwardedMeta,
          pointAwarded.isAcceptableValue(
              d.pointAwarded.value, _pointAwardedMeta));
    } else if (isInserting) {
      context.missing(_pointAwardedMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  PointSetting map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return PointSetting.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(PointSettingsCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.minTransaction.present) {
      map['min_transaction'] = Variable<int, IntType>(d.minTransaction.value);
    }
    if (d.pointAwarded.present) {
      map['point_awarded'] = Variable<int, IntType>(d.pointAwarded.value);
    }
    return map;
  }

  @override
  $PointSettingsTable createAlias(String alias) {
    return $PointSettingsTable(_db, alias);
  }
}

abstract class _$CoreDatabase extends GeneratedDatabase {
  _$CoreDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  $MembersTable _members;
  $MembersTable get members => _members ??= $MembersTable(this);
  $SuppliersTable _suppliers;
  $SuppliersTable get suppliers => _suppliers ??= $SuppliersTable(this);
  $ItemsTable _items;
  $ItemsTable get items => _items ??= $ItemsTable(this);
  $ItemUnitsTable _itemUnits;
  $ItemUnitsTable get itemUnits => _itemUnits ??= $ItemUnitsTable(this);
  $UserTransactionsTable _userTransactions;
  $UserTransactionsTable get userTransactions =>
      _userTransactions ??= $UserTransactionsTable(this);
  $PaymentsTable _payments;
  $PaymentsTable get payments => _payments ??= $PaymentsTable(this);
  $PointSettingsTable _pointSettings;
  $PointSettingsTable get pointSettings =>
      _pointSettings ??= $PointSettingsTable(this);
  SupplierDao _supplierDao;
  SupplierDao get supplierDao =>
      _supplierDao ??= SupplierDao(this as CoreDatabase);
  MemberDao _memberDao;
  MemberDao get memberDao => _memberDao ??= MemberDao(this as CoreDatabase);
  ItemDao _itemDao;
  ItemDao get itemDao => _itemDao ??= ItemDao(this as CoreDatabase);
  ItemUnitDao _itemUnitDao;
  ItemUnitDao get itemUnitDao =>
      _itemUnitDao ??= ItemUnitDao(this as CoreDatabase);
  UserTransactionDao _userTransactionDao;
  UserTransactionDao get userTransactionDao =>
      _userTransactionDao ??= UserTransactionDao(this as CoreDatabase);
  PaymentDao _paymentDao;
  PaymentDao get paymentDao => _paymentDao ??= PaymentDao(this as CoreDatabase);
  PointSettingDao _pointSettingDao;
  PointSettingDao get pointSettingDao =>
      _pointSettingDao ??= PointSettingDao(this as CoreDatabase);
  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [
        members,
        suppliers,
        items,
        itemUnits,
        userTransactions,
        payments,
        pointSettings
      ];
}
