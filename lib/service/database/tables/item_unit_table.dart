import 'package:moor/moor.dart';

@DataClassName('ItemUnit')
class ItemUnits extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get name => text()();
  @override
  Set<Column> get primaryKey => {id};
}
