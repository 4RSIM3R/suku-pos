import 'package:moor/moor.dart';

@DataClassName('Supplier')
class Suppliers extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get name => text()();
  TextColumn get address => text()();
  TextColumn get phone => text()();

  @override
  Set<Column> get primaryKey => {id};
}