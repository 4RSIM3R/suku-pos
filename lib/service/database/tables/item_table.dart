import 'package:moor/moor.dart';

@DataClassName('Item')
class Items extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get name => text()();
  TextColumn get barcode => text()();
  TextColumn get unit => text()();
  IntColumn get priceLevel1 => integer()();
  IntColumn get priceLevel2 => integer()();
  IntColumn get priceLevel3 => integer()();
  IntColumn get minPriceLevel1 => integer()();
  IntColumn get minPriceLevel2 => integer()();
  IntColumn get minPriceLevel3 => integer()();
  IntColumn get discount => integer()();
  IntColumn get price => integer()();
  IntColumn get stock => integer()();

  @override
  Set<Column> get primaryKey => {id};

}