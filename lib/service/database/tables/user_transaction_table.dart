import 'package:moor/moor.dart';

class UserTransactions extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get code => text()();
  TextColumn get name => text()();
  IntColumn get quantity => integer()();
  IntColumn get unitPrice => integer()();
  IntColumn get totalUnitPrice => integer()();
  DateTimeColumn get date => dateTime()();
  @override
  Set<Column> get primaryKey => {id};
}
