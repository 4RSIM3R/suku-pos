import 'package:moor/moor.dart';

class PointSettings extends Table {
  IntColumn get id => integer().autoIncrement()();
  IntColumn get minTransaction => integer()();
  IntColumn get pointAwarded => integer()();
  @override
  Set<Column> get primaryKey => {id};
}