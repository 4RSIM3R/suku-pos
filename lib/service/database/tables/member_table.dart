import 'package:moor/moor.dart';

@DataClassName('Member')
class Members extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get memberNumber => text()();
  TextColumn get name => text()();
  TextColumn get address => text()();
  TextColumn get identityType => text()();
  TextColumn get identityNumber => text()();
  TextColumn get phone => text()();
  TextColumn get type => text()();
  IntColumn get totalPoint => integer()();
  @override
  Set<Column> get primaryKey => {id};
}