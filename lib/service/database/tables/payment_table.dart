import 'package:moor/moor.dart';

class Payments extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get code => text()();
  IntColumn get totalPrice => integer()();
  IntColumn get buyerMoney => integer()();
  IntColumn get buyerCharge => integer()();
  DateTimeColumn get date => dateTime()();

  @override
  Set<Column> get primaryKey => {id};
}
