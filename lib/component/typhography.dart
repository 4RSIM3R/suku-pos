import 'package:flutter/material.dart';
import 'package:toko_ku/constant/constant.dart';

Widget heading1({
  @required String text,
  Color color = black,
  FontWeight fontWeight = FontWeight.w500,
  TextAlign textAlign = TextAlign.left,
  TextOverflow textOverflow = TextOverflow.ellipsis,
  int maxlines = 3
}) {
  return Text(
    text,
    style: TextStyle(
      fontSize: 32,
      color: color,
      fontWeight: fontWeight,
    ),
    textAlign: textAlign,
    overflow: textOverflow,
    maxLines: maxlines,
  );
}

Widget heading2({
  @required String text,
  Color color = black,
  FontWeight fontWeight = FontWeight.w500,
  TextAlign textAlign = TextAlign.left,
  TextOverflow textOverflow = TextOverflow.ellipsis,
  int maxlines = 3
}) {
  return Text(
    text,
    style: TextStyle(
      fontSize: 28,
      color: color,
      fontWeight: fontWeight,
    ),
    textAlign: textAlign,
    overflow: textOverflow,
    maxLines: maxlines,
  );
}

Widget heading3({
  @required String text,
  Color color = black,
  FontWeight fontWeight = FontWeight.w500,
  TextAlign textAlign = TextAlign.left,
  TextOverflow textOverflow = TextOverflow.ellipsis,
  int maxlines = 3
}) {
  return Text(
    text,
    style: TextStyle(
      fontSize: 24,
      color: color,
      fontWeight: fontWeight,
    ),
    textAlign: textAlign,
    overflow: textOverflow,
    maxLines: maxlines,
  );
}

Widget heading4({
  @required String text,
  Color color = black,
  FontWeight fontWeight = FontWeight.w500,
  TextAlign textAlign = TextAlign.left,
  TextOverflow textOverflow = TextOverflow.ellipsis,
  int maxlines = 3
}) {
  return Text(
    text,
    style: TextStyle(
      fontSize: 20,
      color: color,
      fontWeight: fontWeight,
    ),
    textAlign: textAlign,
    overflow: textOverflow,
    maxLines: maxlines,
  );
}

Widget heading5({
  @required String text,
  Color color = grey,
  FontWeight fontWeight = FontWeight.w500,
  TextAlign textAlign = TextAlign.left,
  TextOverflow textOverflow = TextOverflow.ellipsis,
  int maxlines = 3
}) {
  return Text(
    text,
    style: TextStyle(
      fontSize: 16,
      color: color,
      fontWeight: fontWeight,
    ),
    textAlign: textAlign,
    overflow: textOverflow,
    maxLines: maxlines,
  );
}

Widget heading6({
  @required String text,
  Color color = black,
  FontWeight fontWeight = FontWeight.w500,
  TextAlign textAlign = TextAlign.left,
  TextOverflow textOverflow = TextOverflow.ellipsis,
  int maxlines = 3
}) {
  return Text(
    text,
    style: TextStyle(
      fontSize: 12,
      color: color,
      fontWeight: fontWeight,
    ),
    textAlign: textAlign,
    overflow: textOverflow,
    maxLines: maxlines,
  );
}

