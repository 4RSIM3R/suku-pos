import 'package:flutter/material.dart';
import 'package:toko_ku/constant/constant.dart';

Widget baseButton({
  @required String title,
  @required Function() onClick,
  Color textColor = white,
  Color backgroundColor = blue,
  double height = 50.0,
  double width = double.infinity,
  double radius = 5.0,
}) {
  return GestureDetector(
    onTap: onClick,
    child: Container(
      height: height,
      width: width,
      decoration: BoxDecoration(
        color: backgroundColor,
        borderRadius: BorderRadius.circular(radius),
      ),
      child: Center(
        child: Text(
          title,
          style: TextStyle(color: white, fontSize: 18.0),
        ),
      ),
    ),
  );
}

class BaseButton extends StatelessWidget {
  final String title;
  final Function onClick;
  final Color textColor;
  final Color backgroundColor;
  final height;
  final width;
  final radius;
  const BaseButton({
    Key key,
    @required this.title,
    @required this.onClick,
    this.textColor = white,
    this.backgroundColor = blue,
    this.height = 50.0,
    this.width = double.infinity,
    this.radius = 5.0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onClick,
      child: Container(
        height: height,
        width: width,
        decoration: BoxDecoration(
          color: backgroundColor,
          borderRadius: BorderRadius.circular(radius),
        ),
        child: Center(
          child: Text(
            title,
            style: TextStyle(color: white, fontSize: 18.0),
          ),
        ),
      ),
    );
  }
}
