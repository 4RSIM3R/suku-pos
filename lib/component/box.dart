import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:toko_ku/component/typhography.dart';
import 'package:toko_ku/constant/constant.dart';
import 'package:toko_ku/service/database/core_database.dart';
import 'package:toko_ku/utils/generator.dart';

class BoxForList extends StatelessWidget {
  final int index;
  final IconData icon;
  final String title;
  const BoxForList({
    Key key,
    this.index,
    this.icon,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.of(context).pushNamed(listPagesRoutes[index]),
      child: Container(
        padding: EdgeInsets.all(8.0),
        decoration: BoxDecoration(
          color: colorGeneratorList(index),
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Center(
              child: Icon(
                icon,
                color: white,
                size: 32.0,
              ),
            ),
            SizedBox(height: 8.0,),
            heading4(text: title, color: white, textAlign: TextAlign.center)
          ],
        ),
      ),
    );
  }
}

class BoxForItem extends StatelessWidget {
  final Item item;
  BoxForItem({Key key,@required this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: white,
        borderRadius: BorderRadius.circular(5.0),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 2.0,
            offset: Offset(3, 3)
          )
        ]
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Container(
              height: 140,
              width: double.infinity,
              decoration: BoxDecoration(
                color: yellow,
                borderRadius: BorderRadius.circular(5.0),
              ),
              child: Center(
                child: Icon(MdiIcons.storefront, color: white, size: 64.0,),
              ),
            ),
            heading3(text: item.name, fontWeight: FontWeight.w300),
            heading5(text: 'Harga : ${item.priceLevel1}', fontWeight: FontWeight.w300, color: Colors.grey[700]),
            heading5(text: '${item.stock} ${item.unit}', fontWeight: FontWeight.w300, color: Colors.grey[700])
          ],
        ),
      ),
    );
  }
}