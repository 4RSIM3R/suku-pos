import 'package:flutter/material.dart';
import 'package:toko_ku/component/typhography.dart';
import 'package:toko_ku/constant/constant.dart';

PreferredSizeWidget baseHeader({
  @required BuildContext context,
  @required String title,
  Widget leading,
  List<Widget> actions,
  bool centerTitle = false
}) {
  return AppBar(
    backgroundColor: white,
    elevation: 0.0,
    leading: leading,
    title: heading4(text: title),
    centerTitle: centerTitle,
    actions: actions,
  );
}
