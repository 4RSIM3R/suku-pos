import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:toko_ku/bloc/local/member_transaction_bloc.dart';
import 'package:toko_ku/bloc/local/transaction_bloc.dart';
import 'package:toko_ku/bloc/local/validator.dart';
import 'package:toko_ku/constant/routing.dart';
import 'package:toko_ku/routes/routes.dart';
import 'package:toko_ku/service/database/core_database.dart';

void main(){
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider(create: (_) => CoreDatabase()),
        Provider(create: (_) => TransactionBloc()),
        Provider(create: (_) => InputBloc()),
        Provider(create: (_) => MemberTransactionBloc())
      ],
      child: MaterialApp(
        title: 'Suku',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
          fontFamily: "Sans",
        ),
        initialRoute: splashPage,
        onGenerateRoute: Routes().onGenerateRoute,
      ),
    );
  }
}
