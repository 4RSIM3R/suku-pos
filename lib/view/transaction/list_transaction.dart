import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:toko_ku/component/header.dart';
import 'package:toko_ku/component/typhography.dart';
import 'package:toko_ku/constant/constant.dart';
import 'package:toko_ku/service/database/core_database.dart';
import 'package:toko_ku/view/widgets/item_transaction.dart';

class ListTransaction extends StatelessWidget {
  ListTransaction({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final db = Provider.of<CoreDatabase>(context);
    return Scaffold(
      appBar: baseHeader(
        context: context,
        title: 'Daftar Transaksi',
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: black,
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.all(12.0),
        child: StreamBuilder<List<Payment>>(
          stream: db.paymentDao.getAllPayment,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Container(
                child: CircularProgressIndicator(),
              );
            } else if (snapshot.data.length == 0) {
              return Container(
                child: Center(
                  child: heading3(text: 'Belum Ada Transaksi'),
                ),
              );
            }
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (context, index) {
                return ItemTransaction(snapshot: snapshot.data[index]);
              },
            );
          },
        ),
      ),
    );
  }
}
