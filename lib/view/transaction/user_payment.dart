import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:random_string/random_string.dart';
import 'package:toko_ku/bloc/local/transaction_bloc.dart';
import 'package:toko_ku/bloc/local/validator.dart';
import 'package:toko_ku/component/header.dart';
import 'package:toko_ku/component/input.dart';
import 'package:toko_ku/component/typhography.dart';
import 'package:toko_ku/constant/constant.dart';
import 'package:toko_ku/constant/routing.dart';
import 'package:toko_ku/service/database/core_database.dart';
import 'package:toko_ku/utils/show_dialog.dart';
import 'package:toko_ku/view/widgets/dashed_line.dart';
import 'package:toko_ku/view/widgets/user_payment_item.dart';

class UserPayment extends StatefulWidget {
  const UserPayment({Key key}) : super(key: key);

  @override
  _UserPaymentState createState() => _UserPaymentState();
}

class _UserPaymentState extends State<UserPayment> {
  static DateTime now = new DateTime.now();
  static DateFormat formatter = new DateFormat('dd-MM-yyyy');
  String formatted;
  static String transactionCode = '';
  static int buyerMoney = 0;

  @override
  void initState() {
    super.initState();
    formatted = formatter.format(now);
    transactionCode = randomAlphaNumeric(7);
  }

  @override
  Widget build(BuildContext context) {
    final db = Provider.of<CoreDatabase>(context);
    final transactionBloc = Provider.of<TransactionBloc>(context);
    final inputBloc = Provider.of<InputBloc>(context);
    return Scaffold(
      appBar: baseHeader(
        context: context,
        title: 'Pembayaran Pembeli',
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: black,
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.all(12.0),
          padding: EdgeInsets.all(12.0),
          decoration: BoxDecoration(
            color: white,
            borderRadius: BorderRadius.circular(5.0),
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                blurRadius: 3.0,
                offset: Offset(3, 3),
              ),
            ],
          ),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      heading4(text: 'Tanggal'),
                      heading6(text: formatted),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      heading4(text: 'Kode'),
                      heading6(text: transactionCode),
                    ],
                  )
                ],
              ),
              SizedBox(
                height: 12.0,
              ),
              DashedLine(
                height: 2.0,
                color: Colors.black87,
              ),
              SizedBox(
                height: 12.0,
              ),
              StreamBuilder<List<Item>>(
                stream: transactionBloc.listItem,
                builder: (context, snapshot) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: _generateItemWidget(snapshot.data),
                  );
                },
              ),
              SizedBox(
                height: 12.0,
              ),
              DashedLine(
                height: 2.0,
                color: Colors.black87,
              ),
              SizedBox(
                height: 12.0,
              ),
              StreamBuilder<int>(
                  stream: transactionBloc.priceTotal,
                  builder: (context, snapshot) {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        heading4(text: 'Total Belanja : '),
                        heading4(text: snapshot.data.toString()),
                      ],
                    );
                  })
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          showCommonDialog(
            context: context,
            content: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                heading6(text: 'Pembeli masih mengambil barang lagi?\nKamu bisa menyimpannya di draft'),
                StreamBuilder<String>(
                  stream: inputBloc.input,
                  builder: (context, snapshot) {
                    return TextInputWithTitle(
                      inputType: TextInputType.number,
                      title: 'Uang pembeli',
                      errorText: snapshot.hasError ? snapshot.error : null,
                      onChanged: (String val) {
                        buyerMoney = int.parse(val);
                        transactionBloc.userCharge(buyerMoney);
                        inputBloc.changeInput(val);
                      },
                    );
                  }
                ),
                SizedBox(
                  height: 12.0,
                ),
                StreamBuilder<int>(
                    stream: transactionBloc.chargeUser,
                    builder: (context, snapshot) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          heading5(text: 'Kembalian :'),
                          heading5(text: snapshot.data.toString()),
                        ],
                      );
                    })
              ],
            ),
            title: Text('Halo'),
            actions: [
              FlatButton(
                onPressed: () {},
                child: Text('Simpan Ke Draft'),
              ),
              StreamBuilder<List<Item>>(
                stream: transactionBloc.listItem,
                builder: (context, snap) {
                  return StreamBuilder<List<int>>(
                    stream: transactionBloc.listItemUnit,
                    builder: (context, snapshot) {
                      return StreamBuilder<List<int>>(
                        stream: transactionBloc.levelPricePerUnit,
                        builder: (context, levelPricePerUnit) {
                          return StreamBuilder<List<int>>(
                            stream: transactionBloc.priceLevel,
                            builder: (context, priceLevel) {
                              return StreamBuilder<int>(
                                stream: transactionBloc.priceTotal,
                                builder: (context, priceTotal) {
                                  return StreamBuilder<int>(
                                    stream: transactionBloc.chargeUser,
                                    builder: (context, chargeUser) {
                                      return FlatButton(
                                        onPressed: () => processTransaction(
                                          items: snap.data,
                                          sold: snapshot.data,
                                          database: db,
                                          levelPricePerUnit: levelPricePerUnit.data,
                                          priceTotal: priceTotal.data,
                                          priceLevel: priceLevel.data,
                                          chargeUser: chargeUser.data,
                                          bloc: transactionBloc,
                                        ),
                                        child: Text('Cetak Struk'),
                                      );
                                    },
                                  );
                                },
                              );
                            },
                          );
                        },
                      );
                    },
                  );
                },
              ),
            ],
          );
        },
        backgroundColor: blue,
        label: heading5(text: 'Lanjut', color: white),
        icon: Icon(
          Icons.payment,
          color: white,
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  List<Widget> _generateItemWidget(List<Item> items) {
    List<Widget> _generatedItemWidget = [];
    items.asMap().forEach((index, item) {
      _generatedItemWidget.add(UserPaymentItem(
        item: item,
        index: index,
      ));
    });
    return _generatedItemWidget;
  }

  Future<void> processTransaction({
    List<Item> items,
    List<int> sold,
    List<int> levelPricePerUnit,
    List<int> priceLevel,
    int priceTotal,
    int chargeUser,
    CoreDatabase database,
    TransactionBloc bloc,
  }) async {
    List<Item> updatedItems = [];
    List<UserTransaction> userTransaction = [];
    int id;

    Payment payment = Payment(
      id: id,
      code: transactionCode,
      totalPrice: priceTotal,
      buyerMoney: buyerMoney,
      buyerCharge: chargeUser,
      date: DateTime.now()
    );

    items.asMap().forEach((index, val) {
      updatedItems.add(Item(
        id: val.id,
        name: val.name,
        barcode: val.barcode,
        unit: val.unit,
        priceLevel1: val.priceLevel1,
        priceLevel2: val.priceLevel2,
        priceLevel3: val.priceLevel3,
        minPriceLevel1: val.minPriceLevel1,
        minPriceLevel2: val.minPriceLevel2,
        minPriceLevel3: val.minPriceLevel3,
        discount: val.discount,
        price: val.price,
        stock: (val.stock - sold[index]),
      ));

      userTransaction.add(UserTransaction(
        id: id,
        code: transactionCode,
        name: val.name,
        quantity: sold[index],
        unitPrice: levelPricePerUnit[index],
        totalUnitPrice: priceLevel[index],
        date: DateTime.now(),
      ));
    });

    database.paymentDao.addPayment(payment).then((val) {
      print('Berhasil');
    }).catchError((e) {
      print('ADA ERROR : $e');
    });
    try {
      database.userTransactionDao.multipleTransactionInsert(userTransaction);
      updatedItems.forEach((x) => database.itemDao.updateItem(x));
      bloc.dispose();
      Navigator.of(context).pushNamed(homePage);
    } catch (e) {
      print('ADA ERROR : $e');
    }
  }
}
