import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:toko_ku/bloc/local/transaction_bloc.dart';
import 'package:toko_ku/component/clickable.dart';
import 'package:toko_ku/component/header.dart';
import 'package:toko_ku/component/typhography.dart';
import 'package:toko_ku/constant/constant.dart';
import 'package:toko_ku/constant/routing.dart';
import 'package:toko_ku/service/database/core_database.dart';
import 'package:toko_ku/utils/show_dialog.dart';
import 'package:toko_ku/view/widgets/cart_item.dart';

class CartPage extends StatelessWidget {
  CartPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final transactionBloc = Provider.of<TransactionBloc>(context);
    transactionBloc.initCartPage();
    return Scaffold(
      appBar: baseHeader(
        context: context,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: black,
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: 'Daftar Belanjaan',
      ),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Container(
          margin: EdgeInsets.all(12.0),
          padding: EdgeInsets.all(12.0),
          decoration: BoxDecoration(
            color: white,
            borderRadius: BorderRadius.circular(5.0),
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                blurRadius: 3.0,
                offset: Offset(3, 3),
              ),
            ],
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              StreamBuilder<List<Item>>(
                stream: transactionBloc.listItem,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Container(
                      child: CircularProgressIndicator(),
                    );
                  } else if (snapshot.hasData && snapshot.data.length == 0) {
                    return Container(
                      child: Center(
                        child: heading3(text: 'Belum Ada Barang'),
                      ),
                    );
                  }
                  return Column(
                    children: _generateItemWidget(snapshot.data),
                  );
                },
              ),
              Container(
                height: 2.0,
                width: double.infinity,
                decoration: BoxDecoration(color: Colors.black12),
              ),
              SizedBox(
                height: 12.0,
              ),
              StreamBuilder<List<Item>>(
                  stream: transactionBloc.listItem,
                  builder: (context, snapshot) {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        heading3(text: 'Total : '),
                        StreamBuilder<int>(
                          stream: transactionBloc.priceTotal,
                          builder: (context, snapshot) {
                            return heading3(text: snapshot.data.toString());
                          },
                        ),
                      ],
                    );
                  }),
              SizedBox(
                height: 12.0,
              ),
              BaseButton(
                title: 'Bayar',
                onClick: () {
                  showCommonDialog(
                    context: context,
                    title: Text('Halo'),
                    content: Text('Apakah transaksi sudah selesai'),
                    actions: [
                      FlatButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text('Tidak'),
                      ),
                      FlatButton(
                        onPressed: () => Navigator.of(context).pushNamed(userPayment),
                        child: Text('Iya'),
                      ),
                    ],
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  List<Widget> _generateItemWidget(List<Item> items) {
    List<Widget> _generatedItemWidget = [];
    items.asMap().forEach((index, item) {
      _generatedItemWidget.add(
        CartItem(
          item: item,
          index: index,
        ),
      );
    });
    return _generatedItemWidget;
  }
}
