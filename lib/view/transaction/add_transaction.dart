import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:provider/provider.dart';
import 'package:toko_ku/bloc/local/transaction_bloc.dart';
import 'package:toko_ku/component/box.dart';
import 'package:toko_ku/component/header.dart';
import 'package:toko_ku/component/typhography.dart';
import 'package:toko_ku/constant/constant.dart';
import 'package:toko_ku/constant/routing.dart';
import 'package:toko_ku/service/database/core_database.dart';
import 'package:toko_ku/utils/search_delegate/item_search_delegate.dart';

class AddTransaction extends StatefulWidget {
  AddTransaction({Key key}) : super(key: key);

  @override
  _AddTransactionState createState() => _AddTransactionState();
}

class _AddTransactionState extends State<AddTransaction> {
  @override
  Widget build(BuildContext context) {
    final db = Provider.of<CoreDatabase>(context);
    final transactionBloc = Provider.of<TransactionBloc>(context);
    return WillPopScope(
      onWillPop: () async {
        transactionBloc.dispose();
        return true;
      },
      child: Scaffold(
        appBar: baseHeader(
          context: context,
          title: 'Tambah Transaksi',
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: black,
            ),
            onPressed: () {
              Navigator.of(context).pop();
              transactionBloc.dispose();
            },
          ),
          actions: [
            IconButton(
              icon: Icon(
                Icons.search,
                color: black,
              ),
              onPressed: () {
                showSearch(
                  context: context,
                  delegate: ItemSearchDelegate(),
                );
              },
            ),
            Padding(
              padding: EdgeInsets.only(right: 8.0, left: 8.0),
              child: PopupMenuButton<String>(
                child: Icon(
                  Icons.more_vert,
                  color: black,
                ),
                onSelected: (String val) {
                  if (val == 'Riwayat') {
                    Navigator.of(context).pushNamed(listTransaction);
                  } else if (val == 'Transaksi Member') {
                    Navigator.of(context).pushNamed(addTransactionMember);
                  }
                },
                itemBuilder: (BuildContext context) {
                  return {'Riwayat', 'Draft', 'Transaksi Member'}.map((String choice) {
                    return PopupMenuItem<String>(
                      value: choice,
                      child: Text(choice),
                    );
                  }).toList();
                },
              ),
            )
          ],
        ),
        body: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: StreamBuilder<List<Item>>(
                  stream: db.itemDao.getAllItem,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return Container(
                        child: CircularProgressIndicator(),
                      );
                    } else if (snapshot.data.length == 0) {
                      return Container(
                        child: Center(
                          child: heading3(text: 'Belum Ada Barang'),
                        ),
                      );
                    }
                    return StaggeredGridView.countBuilder(
                      physics: BouncingScrollPhysics(),
                      crossAxisCount: 4,
                      itemCount: snapshot.data.length,
                      itemBuilder: (BuildContext context, int index) => GestureDetector(
                        onTap: () {
                          transactionBloc.addItem(snapshot.data[index]);
                        },
                        child: BoxForItem(
                          item: snapshot.data[index],
                        ),
                      ),
                      staggeredTileBuilder: (int index) => new StaggeredTile.count(2, 3),
                      mainAxisSpacing: 8.0,
                      crossAxisSpacing: 8.0,
                    );
                  },
                ),
              )
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: blue,
          onPressed: () {
            Navigator.of(context).pushNamed(cartPage);
          },
          child: StreamBuilder<List<Item>>(
              stream: transactionBloc.listItem,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Icon(
                    Icons.shopping_cart,
                    color: white,
                  );
                } else if (snapshot.data.length != 0) {
                  return Badge(
                    badgeContent: heading6(text: '${snapshot.data.length}', color: white),
                    badgeColor: yellow,
                    position: BadgePosition.bottomLeft(bottom: 12.0),
                    child: Icon(
                      Icons.shopping_cart,
                      color: white,
                    ),
                  );
                }
                return Icon(
                  Icons.shopping_cart,
                  color: white,
                );
              }),
        ),
      ),
    );
  }
}
