import 'package:flutter/material.dart';
import 'package:toko_ku/component/typhography.dart';
import 'package:toko_ku/constant/constant.dart';
import 'package:toko_ku/service/database/core_database.dart';

class MemberItem extends StatelessWidget {

  final Supplier supplier;
  final Member member;
  final String type;

  MemberItem({this.supplier, this.type, this.member});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 10.0,
        vertical: 10.0
      ),
      margin: EdgeInsets.only(
        bottom: 8.0
      ),
      height: 70.0,
      decoration: BoxDecoration(
        color: white,
        borderRadius: BorderRadius.circular(2.0),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            offset: Offset(3, 3),
            blurRadius: 3
          )
        ]
      ),
      width: double.infinity,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            height: 50.0,
            width: 50.0,
            decoration: BoxDecoration(
              color: yellow,
              borderRadius: BorderRadius.circular(5.0)
            ),
          ),
          SizedBox(width: 8.0,),
          Expanded(
            flex: 1,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                heading4(text: type == "supplier" ? supplier.name : member.name),
                heading5(text: type == "supplier" ? supplier.address : member.address),
              ],
            ),
          ),
          SizedBox(width: 8.0,),
          IconButton(icon: Icon(Icons.call, color: grey, size: 24.0,), onPressed: (){})
        ],
      ),
    );
  }
}