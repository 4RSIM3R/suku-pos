import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:toko_ku/bloc/local/transaction_bloc.dart';
import 'package:toko_ku/component/typhography.dart';
import 'package:toko_ku/constant/constant.dart';
import 'package:toko_ku/service/database/core_database.dart';

class CartItem extends StatelessWidget {
  final Item item;
  final int index;

  const CartItem({Key key, this.item, this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final transactionBloc = Provider.of<TransactionBloc>(context);
    return Container(
      margin: EdgeInsets.only(bottom: 12.0),
      child: Row(
        children: <Widget>[
          Container(
            height: 55.0,
            width: 55.0,
            decoration: BoxDecoration(color: yellow, borderRadius: BorderRadius.circular(5.0)),
          ),
          SizedBox(
            width: 8.0,
          ),
          Expanded(
            flex: 1,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                heading4(text: item.name),
                SizedBox(
                  height: 4.0,
                ),
                StreamBuilder<List<int>>(
                  stream: transactionBloc.levelPricePerUnit,
                  builder: (context, snapshot) {
                    return heading5(
                      text: '${snapshot.data[index].toString()}',
                    );
                  },
                ),
              ],
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 12.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      StreamBuilder<List<int>>(
                          stream: transactionBloc.listItemUnit,
                          builder: (context, snapshot) {
                            return Container(
                              height: 30.0,
                              width: 30.0,
                              decoration: BoxDecoration(
                                color: Colors.grey[200],
                                borderRadius: BorderRadius.circular(15),
                              ),
                              child: Center(
                                child: IconButton(
                                  icon: Icon(
                                    MdiIcons.minus,
                                    color: black,
                                    size: 15.0,
                                  ),
                                  onPressed: () {
                                    if (snapshot.data[index] != 1) {
                                      transactionBloc.removeUnit(index);
                                    }
                                  },
                                ),
                              ),
                            );
                          }),
                      StreamBuilder<List<int>>(
                          stream: transactionBloc.listItemUnit,
                          builder: (context, snapshot) {
                            return heading4(text: snapshot.data[index].toString());
                          }),
                      Container(
                        height: 30.0,
                        width: 30.0,
                        decoration: BoxDecoration(
                          color: Colors.grey[200],
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: Center(
                          child: IconButton(
                              icon: Icon(
                                Icons.add,
                                color: black,
                                size: 15.0,
                              ),
                              onPressed: () {
                                transactionBloc.addUnit(index);
                              }),
                        ),
                      ),
                    ],
                  ),
                  StreamBuilder<List<int>>(
                    stream: transactionBloc.priceLevel,
                    builder: (context, snapshot) {
                      return heading5(text: snapshot.data[index].toString());
                    },
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
