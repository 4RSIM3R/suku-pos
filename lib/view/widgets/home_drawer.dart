import 'package:flutter/material.dart';
import 'package:toko_ku/component/typhography.dart';
import 'package:toko_ku/constant/constant.dart';

class HomeDrawer extends StatelessWidget {
  const HomeDrawer({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Scaffold(
        backgroundColor: white,
        body: Padding(
          padding: const EdgeInsets.only(
            top: 28.0,
            left: 12.0,
            right: 12.0,
            bottom: 24.0
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  CircleAvatar(
                    backgroundColor: yellow,
                    maxRadius: 25.0,
                    minRadius: 25.0,
                    child: Center(
                      child: heading5(text: 'IL', color: white),
                    ),
                  ),
                  SizedBox(
                    width: 8.0,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      heading4(text: 'Ilzam Mulkhaq'),
                      heading5(text: 'Premium user', color: Colors.grey[400]),
                    ],
                  )
                ],
              ),
              Expanded(
                flex: 1,
                child: Container(),
              ),
              heading5(text: 'Suku POS', color: Colors.grey[400])
            ],
          ),
        ),
      ),
    );
  }
}
