import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:toko_ku/component/typhography.dart';
import 'package:toko_ku/constant/constant.dart';
import 'package:toko_ku/service/database/core_database.dart';
import 'package:toko_ku/utils/show_dialog.dart';

class ItemTransaction extends StatelessWidget {
  final Payment snapshot;

  ItemTransaction({Key key, this.snapshot}) : super(key: key);
  static DateFormat formatter = new DateFormat('dd-MM-yyyy');

  @override
  Widget build(BuildContext context) {
    final db = Provider.of<CoreDatabase>(context);
    return StreamBuilder<List<UserTransaction>>(
        stream: db.userTransactionDao.getTransactionByCode(snapshot.code),
        builder: (context, transaction) {
          return GestureDetector(
            onTap: () => showCommonDialog(
              context: context,
              content: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Column(
                      children: _generateItemWidget(transaction.data),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Row(
                      children: <Widget>[
                        heading4(text: 'Total : '),
                        heading4(text: snapshot.totalPrice.toString())
                      ],
                    )
                  ],
                ),
              ),
              title: Text('Rincian Transaksi'),
              actions: [
                FlatButton(
                    onPressed: () => Navigator.of(context).pop(),
                    child: Text('Tutup'))
              ],
            ),
            child: Container(
              padding: EdgeInsets.all(12),
              margin: EdgeInsets.only(bottom: 12.0),
              decoration: BoxDecoration(
                  color: white,
                  borderRadius: BorderRadius.circular(5.0),
                  boxShadow: [
                    BoxShadow(offset: Offset(2, 2), color: Colors.black12)
                  ]),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: 50.0,
                    width: 50.0,
                    decoration: BoxDecoration(
                        color: yellow,
                        borderRadius: BorderRadius.circular(5.0)),
                  ),
                  SizedBox(
                    width: 8.0,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      heading5(text: formatter.format(snapshot.date)),
                      heading6(text: snapshot.code)
                    ],
                  ),
                  Flexible(
                    flex: 1,
                    child: Container(),
                  ),
                  heading4(text: snapshot.totalPrice.toString())
                ],
              ),
            ),
          );
        });
  }

  List<Widget> _generateItemWidget(List<UserTransaction> items) {
    List<Widget> _generatedItemWidget = [];
    items.asMap().forEach((index, item) {
      _generatedItemWidget.add(
        Container(
          margin: EdgeInsets.only(bottom: 8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              heading4(text: '${item.name} X ${item.quantity}'),
              heading4(text: item.totalUnitPrice.toString())
            ],
          ),
        ),
      );
    });
    return _generatedItemWidget;
  }
}
