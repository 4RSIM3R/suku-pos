import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:toko_ku/bloc/local/transaction_bloc.dart';
import 'package:toko_ku/component/typhography.dart';
import 'package:toko_ku/service/database/core_database.dart';

class UserPaymentItem extends StatelessWidget {
  final Item item;
  final int index;
  UserPaymentItem({Key key, this.item, this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final transactionBloc = Provider.of<TransactionBloc>(context);
    return Container(
      margin: EdgeInsets.only(bottom: 8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              StreamBuilder<List<int>>(
                  stream: transactionBloc.listItemUnit,
                  builder: (context, snapshot) {
                    return heading4(text: '${item.name} X ${snapshot.data[index].toString()}');
                  }),
              StreamBuilder<List<int>>(
                stream: transactionBloc.levelPricePerUnit,
                builder: (context, snapshot) {
                  return heading5(text: '${snapshot.data[index].toString()}');
                },
              )
            ],
          ),
          StreamBuilder<List<int>>(
              stream: transactionBloc.priceLevel,
              builder: (context, snapshot) {
                return heading3(text: snapshot.data[index].toString());
              })
        ],
      ),
    );
  }
}
