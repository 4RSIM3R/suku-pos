import 'package:flutter/material.dart';
import 'package:toko_ku/component/typhography.dart';

class SoonPage extends StatelessWidget {
  const SoonPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: heading2(text: 'Soon page'),
      ),
    );
  }
}