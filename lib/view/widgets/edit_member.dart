import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:toast/toast.dart';
import 'package:toko_ku/bloc/local/validator.dart';
import 'package:toko_ku/component/clickable.dart';
import 'package:toko_ku/component/header.dart';
import 'package:toko_ku/component/input.dart';
import 'package:toko_ku/constant/constant.dart';
import 'package:toko_ku/service/database/core_database.dart';

class EditMember extends StatefulWidget {
  EditMember({Key key, @required this.argument}) : super(key: key);
  final EditMemberArgument argument;

  @override
  _EditMemberState createState() => _EditMemberState();
}

class _EditMemberState extends State<EditMember> {
  static int id;
  static String memberNumber, name, address, identityNumber, phone;
  static String type; // Di isi initState
  static String identityType; // Di isi initState
  static int totalPoint;

  TextEditingController memberNumberController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController identityNumberController = TextEditingController();
  TextEditingController phoneController = TextEditingController();

  @override
  void initState() {
    super.initState();
    memberNumberController.text = widget.argument.memberNumber;
    nameController.text = widget.argument.name;
    addressController.text = widget.argument.address;
    identityNumberController.text = widget.argument.identityNumber;
    phoneController.text = widget.argument.phone;
    type = widget.argument.type;
    identityType = widget.argument.identityType;
    id = widget.argument.id;
    totalPoint = widget.argument.totalPoint;
  }

  @override
  Widget build(BuildContext context) {
    CoreDatabase _coreDatabase = Provider.of<CoreDatabase>(context);
    final inputBloc = Provider.of<InputBloc>(context);
    return Scaffold(
      appBar: baseHeader(
        context: context,
        title: 'Edit Member',
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: black,
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Container(
          width: double.infinity,
          padding: EdgeInsets.all(12.0),
          margin: EdgeInsets.all(12.0),
          decoration: BoxDecoration(
            color: white,
            borderRadius: BorderRadius.circular(5.0),
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                blurRadius: 3.0,
                offset: Offset(4, 4),
              ),
            ],
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              StreamBuilder<String>(
                  stream: inputBloc.input,
                  builder: (context, snapshot) {
                    return TextInputWithTitle(
                      title: 'Member ID',
                      errorText: snapshot.hasError ? snapshot.error : null,
                      onChanged: (String val) {
                        memberNumber = val;
                        inputBloc.changeInput(val);
                      },
                      controller: memberNumberController,
                    );
                  }),
              SizedBox(
                height: 12.0,
              ),
              StreamBuilder<String>(
                  stream: inputBloc.input,
                  builder: (context, snapshot) {
                    return TextInputWithTitle(
                      title: 'Nama',
                      errorText: snapshot.hasError ? snapshot.error : null,
                      onChanged: (String val) {
                        name = val;
                        inputBloc.changeInput(val);
                      },
                      controller: nameController,
                    );
                  }),
              SizedBox(
                height: 12.0,
              ),
              StreamBuilder<String>(
                  stream: inputBloc.input,
                  builder: (context, snapshot) {
                    return TextInputWithTitle(
                      title: 'Alamat',
                      errorText: snapshot.hasError ? snapshot.error : null,
                      onChanged: (String val) {
                        address = val;
                        inputBloc.changeInput(val);
                      },
                      controller: addressController,
                    );
                  }),
              SizedBox(
                height: 12.0,
              ),
              BaseDropdown(
                list: ['KTP', 'SIM', 'PASPOR'],
                title: 'Tipe identitas',
                value: identityType,
                onChanged: (String val) {
                  setState(() => identityType = val);
                },
                hint: Text('Tipe identitas'),
              ),
              SizedBox(
                height: 12.0,
              ),
              StreamBuilder<String>(
                  stream: inputBloc.input,
                  builder: (context, snapshot) {
                    return TextInputWithTitle(
                      title: 'Nomor identitas',
                      errorText: snapshot.hasError ? snapshot.error : null,
                      onChanged: (String val) {
                        identityNumber = val;
                        inputBloc.changeInput(val);
                      },
                      controller: identityNumberController,
                    );
                  }),
              SizedBox(
                height: 12.0,
              ),
              StreamBuilder<String>(
                  stream: inputBloc.input,
                  builder: (context, snapshot) {
                    return TextInputWithTitle(
                      title: 'Nomor telepon',
                      errorText: snapshot.hasError ? snapshot.error : null,
                      onChanged: (String val) {
                        phone = val;
                        inputBloc.changeInput(val);
                      },
                      controller: phoneController,
                    );
                  }),
              SizedBox(
                height: 12.0,
              ),
              BaseDropdown(
                list: ['Umum', 'Khusus'],
                title: 'Tipe member',
                value: type,
                onChanged: (String val) {
                  setState(() => type = val);
                },
                hint: Text('Tipe Member'),
              ),
              SizedBox(
                height: 12.0,
              ),
              BaseButton(
                title: 'Ubah',
                onClick: () {
                  _coreDatabase.memberDao
                      .updateMember(
                    Member(
                      id: id,
                      memberNumber: memberNumberController.text,
                      name: nameController.text,
                      address: addressController.text,
                      identityType: identityType,
                      identityNumber: identityNumberController.text,
                      phone: phoneController.text,
                      type: type,
                      totalPoint: totalPoint,
                    ),
                  )
                      .then((val) {
                    Toast.show('Update Member Berhasil', context,
                        gravity: Toast.BOTTOM);
                    Navigator.of(context).pop();
                  }).catchError((e) {
                    Toast.show("Ada Error, Hubungi Developer", context,
                        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                    print('Error :  $e');
                    //Navigator.of(context).pop();
                  });
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}

class EditMemberArgument {
  final int id;
  final String memberNumber;
  final String name;
  final String address;
  final String identityNumber;
  final String phone;
  final String type;
  final String identityType;
  final int totalPoint;

  EditMemberArgument({
    this.id,
    this.memberNumber,
    this.name,
    this.address,
    this.identityNumber,
    this.phone,
    this.type,
    this.identityType,
    this.totalPoint,
  });
}
