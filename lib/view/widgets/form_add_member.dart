import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:toast/toast.dart';
import 'package:toko_ku/bloc/local/validator.dart';
import 'package:toko_ku/component/clickable.dart';
import 'package:toko_ku/component/input.dart';
import 'package:toko_ku/service/database/core_database.dart';

class FormAddMember extends StatefulWidget {
  FormAddMember({Key key}) : super(key: key);

  @override
  _FormAddMemberState createState() => _FormAddMemberState();
}

class _FormAddMemberState extends State<FormAddMember> {
  static int id;
  static String memberNumber, name, address, identityNumber, phone;
  static String type = 'Khusus';
  static String identityType = 'KTP';

  @override
  Widget build(BuildContext context) {
    CoreDatabase _coreDatabase = Provider.of<CoreDatabase>(context);
    final inputBloc = Provider.of<InputBloc>(context);
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        StreamBuilder(
            stream: null,
            builder: (context, snapshot) {
              return TextInputWithTitle(
                title: 'Member ID',
                onChanged: (String val) => memberNumber = val,
                inputType: TextInputType.number,
              );
            }),
        SizedBox(
          height: 12.0,
        ),
        StreamBuilder<String>(
            stream: inputBloc.input,
            builder: (context, snapshot) {
              return TextInputWithTitle(
                title: 'Nama',
                errorText: snapshot.hasError ? snapshot.error : null,
                onChanged: (String val) {
                  name = val;
                  inputBloc.changeInput(val);
                },
              );
            }),
        SizedBox(
          height: 12.0,
        ),
        StreamBuilder<String>(
            stream: inputBloc.input,
            builder: (context, snapshot) {
              return TextInputWithTitle(
                title: 'Alamat',
                errorText: snapshot.hasError ? snapshot.error : null,
                onChanged: (String val) {
                  address = val;
                  inputBloc.changeInput(val);
                },
              );
            }),
        SizedBox(
          height: 12.0,
        ),
        BaseDropdown(
          list: ['KTP', 'SIM', 'PASPOR'],
          title: 'Tipe identitas',
          value: identityType,
          onChanged: (dynamic val) {
            setState(() => identityType = val);
          },
          hint: Text('Tipe identitas'),
        ),
        SizedBox(
          height: 12.0,
        ),
        StreamBuilder<String>(
            stream: inputBloc.input,
            builder: (context, snapshot) {
              return TextInputWithTitle(
                title: 'Nomor identitas',
                errorText: snapshot.hasError ? snapshot.error : null,
                onChanged: (String val) {
                  identityNumber = val;
                  inputBloc.changeInput(val);
                },
                inputType: TextInputType.number,
              );
            }),
        SizedBox(
          height: 12.0,
        ),
        StreamBuilder<String>(
            stream: inputBloc.input,
            builder: (context, snapshot) {
              return TextInputWithTitle(
                title: 'Nomor telepon',
                errorText: snapshot.hasError ? snapshot.error : null,
                onChanged: (String val) {
                  phone = val;
                  inputBloc.changeInput(val);
                },
                inputType: TextInputType.phone,
              );
            }),
        SizedBox(
          height: 12.0,
        ),
        BaseDropdown(
          list: ['Umum', 'Khusus'],
          title: 'Tipe member',
          value: type,
          onChanged: (dynamic val) {
            setState(() => type = val);
          },
          hint: Text('Tipe Member'),
        ),
        SizedBox(
          height: 12.0,
        ),
        BaseButton(
          title: 'Tambah',
          onClick: () {
            _coreDatabase.memberDao
                .addMember(
              Member(
                id: id,
                memberNumber: memberNumber,
                name: name,
                address: address,
                identityType: identityType,
                identityNumber: identityNumber,
                phone: phone,
                type: type,
                totalPoint: 0,
              ),
            )
                .then((val) {
              Toast.show("Tambah Member Sukses", context,
                  duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
              Navigator.of(context).pop();
            }).catchError((e) {
              Toast.show("Ada Error, Hubungi Developer", context,
                  duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
              Navigator.of(context).pop();
            });
          },
        )
      ],
    );
  }
}
