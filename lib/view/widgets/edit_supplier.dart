import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:toast/toast.dart';
import 'package:toko_ku/bloc/local/validator.dart';
import 'package:toko_ku/component/clickable.dart';
import 'package:toko_ku/component/header.dart';
import 'package:toko_ku/component/input.dart';
import 'package:toko_ku/constant/constant.dart';
import 'package:toko_ku/service/database/core_database.dart';

class EditSupplier extends StatefulWidget {
  EditSupplier({Key key, @required this.arg}) : super(key: key);
  final EditSupplierArgument arg;

  @override
  _EditSupplierState createState() => _EditSupplierState();
}

class _EditSupplierState extends State<EditSupplier> {
  static int id;
  static String name, address, phone;

  TextEditingController nameController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController phoneController = TextEditingController();

  @override
  void initState() {
    super.initState();
    nameController.text = widget.arg.name;
    addressController.text = widget.arg.address;
    phoneController.text = widget.arg.phone;
    id = widget.arg.id;
  }

  @override
  Widget build(BuildContext context) {
    final inputBloc = Provider.of<InputBloc>(context);
    CoreDatabase _coreDatabase = Provider.of<CoreDatabase>(context);
    return Scaffold(
      appBar: baseHeader(
        context: context,
        title: 'Edit Supplier',
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: black,
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Container(
        width: double.infinity,
        padding: EdgeInsets.all(12.0),
        margin: EdgeInsets.all(12.0),
        decoration: BoxDecoration(
          color: white,
          borderRadius: BorderRadius.circular(5.0),
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 3.0,
              offset: Offset(4, 4),
            ),
          ],
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            StreamBuilder<String>(
                stream: inputBloc.input,
                builder: (context, snapshot) {
                  return TextInputWithTitle(
                    title: 'Nama',
                    errorText: snapshot.hasError ? snapshot.error : null,
                    onChanged: (String val) {
                      name = val;
                      inputBloc.changeInput(val);
                    },
                    controller: nameController,
                  );
                }),
            SizedBox(
              height: 12.0,
            ),
            StreamBuilder<String>(
                stream: inputBloc.input,
                builder: (context, snapshot) {
                  return TextInputWithTitle(
                    title: 'Alamat',
                    errorText: snapshot.hasError ? snapshot.error : null,
                    onChanged: (String val) {
                      address = val;
                      inputBloc.changeInput(val);
                    },
                    controller: addressController,
                  );
                }),
            SizedBox(
              height: 12.0,
            ),
            StreamBuilder<String>(
                stream: inputBloc.input,
                builder: (context, snapshot) {
                  return TextInputWithTitle(
                    title: 'Nomor Telepon',
                    errorText: snapshot.hasError ? snapshot.error : null,
                    onChanged: (String val) {
                      phone = val;
                      inputBloc.changeInput(val);
                    },
                    controller: phoneController,
                    inputType: TextInputType.phone,
                  );
                }),
            SizedBox(
              height: 12.0,
            ),
            BaseButton(
              title: 'Ubah',
              onClick: () {
                _coreDatabase.supplierDao
                    .updateSupplier(Supplier(
                  id: id,
                  name: nameController.text,
                  address: addressController.text,
                  phone: phoneController.text,
                ))
                    .then((val) {
                  Toast.show('Update Supplier Berhasil', context,
                      gravity: Toast.BOTTOM);
                  Navigator.of(context).pop();
                }).catchError((e) {
                  Toast.show("Ada Error, Hubungi Developer", context,
                      duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                  print('Error :  $e');
                  Navigator.of(context).pop();
                });
              },
            )
          ],
        ),
      ),
    );
  }
}

class EditSupplierArgument {
  final int id;
  final String name;
  final String address;
  final String phone;

  EditSupplierArgument({this.id, this.name, this.address, this.phone});
}
