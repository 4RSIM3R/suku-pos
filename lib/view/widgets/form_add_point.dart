import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:toast/toast.dart';
import 'package:toko_ku/bloc/local/validator.dart';
import 'package:toko_ku/component/clickable.dart';
import 'package:toko_ku/component/input.dart';
import 'package:toko_ku/service/database/core_database.dart';

class FormAddPoint extends StatelessWidget {
  static int id, minTransaction, pointAwarded;

  @override
  Widget build(BuildContext context) {
    final db = Provider.of<CoreDatabase>(context);
    final inputBloc = Provider.of<InputBloc>(context);
    int id;
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        StreamBuilder(
            stream: inputBloc.input,
            builder: (context, snapshot) {
              return TextInputWithTitle(
                title: 'Minimal Transaksi',
                onChanged: (val) {
                  minTransaction = int.parse(val);
                  inputBloc.changeInput(val);
                },
                inputType: TextInputType.number,
              );
            }),
        SizedBox(
          height: 12.0,
        ),
        StreamBuilder(
            stream: inputBloc.input,
            builder: (context, snapshot) {
              return TextInputWithTitle(
                title: 'Point',
                onChanged: (val) {
                  pointAwarded = int.parse(val);
                  inputBloc.changeInput(val);
                },
                inputType: TextInputType.number,
              );
            }),
        SizedBox(
          height: 12.0,
        ),
        BaseButton(
          title: "Tambah",
          onClick: () {
            db.pointSettingDao
                .addPointSetting(
                  PointSetting(id: id, minTransaction: minTransaction, pointAwarded: pointAwarded),
                )
                .then((val) {
                  Toast.show('Tambah Poin Sukses', context);
                  Navigator.of(context).pop();
                })
                .catchError((e) {
                  Toast.show('Ada Error Hubungi Developer', context);
                });
          },
        )
      ],
    );
  }
}
