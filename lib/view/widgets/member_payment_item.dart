import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:toko_ku/bloc/local/member_transaction_bloc.dart';
import 'package:toko_ku/component/typhography.dart';
import 'package:toko_ku/service/database/core_database.dart';

class MemberPaymentItem extends StatelessWidget {
  final Item item;
  final int index;
  const MemberPaymentItem({Key key, this.item, this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of<MemberTransactionBloc>(context);
    return Container(
      margin: EdgeInsets.only(bottom: 8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          StreamBuilder(
            stream: bloc.listItemUnit,
            builder: (BuildContext context, AsyncSnapshot listItemUnit) {
              return StreamBuilder<List<bool>>(
                stream: bloc.halfOrFull,
                builder: (context, hof) {
                  return heading4(text: '${item.name} (${khof(hof.data[index])}) : ');
                },
              );
            },
          ),
          StreamBuilder(
            stream: bloc.listItemUnit,
            builder: (BuildContext context, AsyncSnapshot listItemUnit) {
              return StreamBuilder(
                stream: bloc.levelPricePerUnit,
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  return StreamBuilder<List<int>>(
                    stream: bloc.priceLevel,
                    builder: (context, priceLevel) {
                      return Container(
                        child: heading4(text: '${listItemUnit.data[index]} X ${snapshot.data[index]} (${priceLevel.data[index]})'),
                      );
                    }
                  );
                },
              );
            },
          )
        ],
      ),
    );
  }

  String khof(bool val) => val ? 'Full' : 'Half';
}
