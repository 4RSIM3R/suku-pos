import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:toko_ku/bloc/local/member_transaction_bloc.dart';
import 'package:toko_ku/component/typhography.dart';
import 'package:toko_ku/constant/constant.dart';
import 'package:toko_ku/service/database/core_database.dart';

class CartMemberItem extends StatelessWidget {
  final Item item;
  final int index;

  const CartMemberItem({Key key, this.item, this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of<MemberTransactionBloc>(context);
    return Container(
      margin: EdgeInsets.only(bottom: 12.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                heading4(text: item.name),
                SizedBox(
                  height: 4.0,
                ),
                StreamBuilder<List<int>>(
                  stream: bloc.levelPricePerUnit,
                  builder: (context, snapshot) {
                    return heading5(
                      text: '${snapshot.data[index].toString()}',
                    );
                  },
                ),
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    StreamBuilder<List<int>>(
                        stream: bloc.listItemUnit,
                        builder: (context, snapshot) {
                          return Container(
                            height: 30.0,
                            width: 30.0,
                            decoration: BoxDecoration(
                              color: Colors.grey[200],
                              borderRadius: BorderRadius.circular(15),
                            ),
                            child: Center(
                              child: IconButton(
                                icon: Icon(
                                  MdiIcons.minus,
                                  color: black,
                                  size: 15.0,
                                ),
                                onPressed: () {
                                  if (snapshot.data[index] != 1) {
                                    bloc.removeUnit(index);
                                  }
                                },
                              ),
                            ),
                          );
                        }),
                    StreamBuilder<List<int>>(
                        stream: bloc.listItemUnit,
                        builder: (context, snapshot) {
                          return heading4(text: snapshot.data[index].toString());
                        }),
                    Container(
                      height: 30.0,
                      width: 30.0,
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: Center(
                        child: IconButton(
                          icon: Icon(
                            Icons.add,
                            color: black,
                            size: 15.0,
                          ),
                          onPressed: () {
                            bloc.addUnit(index);
                          },
                        ),
                      ),
                    ),
                  ],
                ),
                StreamBuilder<List<int>>(
                  stream: bloc.priceLevel,
                  builder: (context, snapshot) {
                    return heading5(text: snapshot.data[index].toString());
                  },
                )
              ],
            ),
          ),
          StreamBuilder<List<bool>>(
              stream: bloc.halfOrFull,
              builder: (context, snapshot) {
                return GestureDetector(
                  onTap: () {
                    bloc.changeHalfOrFull(index);
                  },
                  child: Container(
                    height: 50.0,
                    width: 50.0,
                    margin: EdgeInsets.only(left: 28.0),
                    decoration: BoxDecoration(
                      color: snapshot.data[index] ? orange : blue,
                      borderRadius: BorderRadius.circular(25.0),
                    ),
                    child: Center(
                      child: heading3(text: snapshot.data[index] ? 'F' : 'H', color: white),
                    ),
                  ),
                );
              })
        ],
      ),
    );
  }
}
