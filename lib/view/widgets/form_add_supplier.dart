import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:toast/toast.dart';
import 'package:toko_ku/bloc/local/validator.dart';
import 'package:toko_ku/component/clickable.dart';
import 'package:toko_ku/component/input.dart';
import 'package:toko_ku/service/database/core_database.dart';

class FormAddSupplier extends StatefulWidget {
  const FormAddSupplier({Key key}) : super(key: key);

  @override
  _FormAddSupplierState createState() => _FormAddSupplierState();
}

class _FormAddSupplierState extends State<FormAddSupplier> {
  static int id;
  static String name, address, phone;
  //CoreDatabase _coreDatabase;

  @override
  void initState() {
    //_coreDatabase = CoreDatabase();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    CoreDatabase _coreDatabase = Provider.of<CoreDatabase>(context);
    final inputBloc = Provider.of<InputBloc>(context);
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        StreamBuilder<String>(
          stream: inputBloc.input,
          builder: (context, snapshot) {
            return TextInputWithTitle(
              title: 'Nama',
              errorText: snapshot.hasError ? snapshot.error : null,
              onChanged: (String val) {
                name = val;
                inputBloc.changeInput(val);
              },
            );
          }
        ),
        SizedBox(
          height: 12.0,
        ),
        StreamBuilder<String>(
          stream: inputBloc.input,
          builder: (context, snapshot) {
            return TextInputWithTitle(
              title: 'Alamat',
              errorText: snapshot.hasError ? snapshot.error : null,
              onChanged: (String val) {
                address = val;
                inputBloc.changeInput(val);
              },
            );
          }
        ),
        SizedBox(
          height: 12.0,
        ),
        StreamBuilder<String>(
          stream: inputBloc.input,
          builder: (context, snapshot) {
            return TextInputWithTitle(
              title: 'Nomor Telepon',
              errorText: snapshot.hasError ? snapshot.error : null,
              onChanged: (String val) {
                phone = val;
                inputBloc.changeInput(val);
              },
              inputType: TextInputType.phone,
            );
          }
        ),
        SizedBox(
          height: 12.0,
        ),
        BaseButton(
          title: 'Tambah',
          onClick: () {
            _coreDatabase
                .supplierDao.addSupplier(
              Supplier(id: id, name: name, phone: phone, address: address),
            ).then((val) {
              Toast.show("Tambah Supplier Sukses", context,
                  duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
              Navigator.of(context).pop();
            }).catchError((e) {
              Toast.show("Ada Error, Hubungi Developer", context,
                  duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
              Navigator.of(context).pop();
            });
          },
        )
      ],
    );
  }
}
