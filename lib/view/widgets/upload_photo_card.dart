import 'package:flutter/material.dart';
import 'package:toko_ku/component/typhography.dart';
import 'package:toko_ku/constant/constant.dart';

class UploadPhotoCard extends StatelessWidget {
  const UploadPhotoCard({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 200.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5.0),
        color: Colors.grey[300],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Icon(
            Icons.add_a_photo,
            color: white,
            size: 64,
          ),
          heading5(text: 'Upload foto produk', color: white)
        ],
      ),
    );
  }
}

class PhotoCard extends StatelessWidget {
  const PhotoCard({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 65.0,
      width: 65.0,
      margin: EdgeInsets.only(
        right: 8.0
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5.0),
        color: Colors.grey[300],
      ),
    );
  }
}
