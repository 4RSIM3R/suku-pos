import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:random_string/random_string.dart';
import 'package:toko_ku/bloc/local/member_transaction_bloc.dart';
import 'package:toko_ku/bloc/local/validator.dart';
import 'package:toko_ku/component/header.dart';
import 'package:toko_ku/component/input.dart';
import 'package:toko_ku/component/typhography.dart';
import 'package:toko_ku/constant/constant.dart';
import 'package:toko_ku/service/database/core_database.dart';
import 'package:toko_ku/service/database/tables/point_setting_table.dart';
import 'package:toko_ku/utils/show_dialog.dart';
import 'package:toko_ku/view/widgets/dashed_line.dart';
import 'package:toko_ku/view/widgets/member_payment_item.dart';

class PaymentMember extends StatefulWidget {
  const PaymentMember({Key key}) : super(key: key);

  @override
  _PaymentMemberState createState() => _PaymentMemberState();
}

class _PaymentMemberState extends State<PaymentMember> {
  static DateTime now = new DateTime.now();
  static DateFormat formatter = new DateFormat('dd-MM-yyyy');
  String formatted;
  static String transactionCode = '';
  static int buyerMoney = 0;
  static int totalTransaction = 0;
  static Member updatedMember;
  static int pointAwarded = 0;

  @override
  void initState() {
    super.initState();
    formatted = formatter.format(now);
    transactionCode = randomAlphaNumeric(7);
  }

  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of<MemberTransactionBloc>(context);
    final inputBloc = Provider.of<InputBloc>(context);
    final db = Provider.of<CoreDatabase>(context);
    return Scaffold(
      appBar: baseHeader(
        context: context,
        title: 'Pembayaran Member',
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: black,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.all(12.0),
          padding: EdgeInsets.all(12.0),
          decoration: BoxDecoration(
            color: white,
            borderRadius: BorderRadius.circular(5.0),
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                blurRadius: 3.0,
                offset: Offset(3, 3),
              ),
            ],
          ),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      heading4(text: 'Tanggal'),
                      heading6(text: formatted),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      heading4(text: 'Kode'),
                      heading6(text: transactionCode),
                    ],
                  )
                ],
              ),
              SizedBox(
                height: 12.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  heading5(text: 'Member : ${bloc.memberBuyer.name}'),
                  heading5(text: 'Type : ${bloc.memberBuyer.type}'),
                ],
              ),
              SizedBox(
                height: 12.0,
              ),
              DashedLine(
                height: 2.0,
                color: Colors.black87,
              ),
              SizedBox(
                height: 12.0,
              ),
              StreamBuilder(
                stream: bloc.listItem,
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: _generateItemWidget(snapshot.data),
                  );
                },
              ),
              SizedBox(
                height: 12.0,
              ),
              DashedLine(
                height: 2.0,
                color: Colors.black87,
              ),
              SizedBox(
                height: 12.0,
              ),
              StreamBuilder<int>(
                stream: bloc.priceTotal,
                builder: (context, snapshot) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      heading4(text: 'Total Belanja : '),
                      heading4(text: snapshot.data.toString()),
                    ],
                  );
                },
              ),
              SizedBox(
                height: 12.0,
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () => showCommonDialog(
          context: context,
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              heading6(text: 'Member masih mengambil barang lagi?\nKamu bisa menyimpannya di draft'),
              TextInputWithTitle(
                inputType: TextInputType.number,
                title: 'Uang Member',
                onChanged: (String val) {
                  buyerMoney = int.parse(val);
                  bloc.userCharge(buyerMoney);
                  inputBloc.changeInput(val);
                },
              ),
              SizedBox(
                height: 12.0,
              ),
              StreamBuilder<int>(
                  stream: bloc.chargeUser,
                  builder: (context, snapshot) {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        heading5(text: 'Kembalian :'),
                        heading5(text: snapshot.data.toString()),
                      ],
                    );
                  })
            ],
          ),
          title: Text('Halo'),
          actions: [
            FlatButton(
              onPressed: () {},
              child: Text('Simpan Ke Draft'),
            ),
            StreamBuilder<List<PointSetting>>(
              stream: db.pointSettingDao.getAllPointSettings,
              builder: (context, snapshot) {
                return FlatButton(
                  onPressed: () => _processTransaction(
                    database: db,
                    bloc: bloc,
                    points: snapshot.data,
                  ),
                  child: Text('Cetak Struk'),
                );
              },
            ),
          ],
        ),
        backgroundColor: blue,
        label: heading5(text: 'Lanjut', color: white),
        icon: Icon(
          Icons.payment,
          color: white,
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  List<Widget> _generateItemWidget(List<Item> items) {
    List<Widget> _generatedItemWidget = [];
    items.asMap().forEach((index, item) {
      _generatedItemWidget.add(MemberPaymentItem(
        index: index,
        item: item,
      ));
    });
    return _generatedItemWidget;
  }

  Future<void> _processTransaction({
    CoreDatabase database,
    MemberTransactionBloc bloc,
    List<PointSetting> points,
  }) async {
    for (var point in points) {
      if (point.minTransaction <= totalTransaction) {
        pointAwarded = point.pointAwarded;
        print('Point : ${point.pointAwarded}, Min Transaction : ${point.minTransaction}');
      }
    }

    // Updated Member
    // updatedMember = Member(
    //   id: bloc.memberValue.id,
    //   memberNumber: bloc.memberValue.memberNumber,
    //   name: bloc.memberValue.name,
    //   address: bloc.memberValue.address,
    //   identityType: bloc.memberValue.identityType,
    //   identityNumber: bloc.memberValue.identityNumber,
    //   phone: bloc.memberValue.phone,
    //   type: bloc.memberValue.type,
    //   totalPoint: bloc.memberValue.totalPoint + pointAwarded,
    // );
  }
}
