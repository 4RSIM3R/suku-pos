import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:provider/provider.dart';
import 'package:toast/toast.dart';
import 'package:toko_ku/bloc/local/member_transaction_bloc.dart';
import 'package:toko_ku/component/box.dart';
import 'package:toko_ku/component/header.dart';
import 'package:toko_ku/component/typhography.dart';
import 'package:toko_ku/constant/constant.dart';
import 'package:toko_ku/constant/routing.dart';
import 'package:toko_ku/service/database/core_database.dart';
import 'package:toko_ku/utils/search_delegate/transaction_member_search.dart';

class AddTransactionMember extends StatelessWidget {
  const AddTransactionMember({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final db = Provider.of<CoreDatabase>(context);
    final bloc = Provider.of<MemberTransactionBloc>(context);
    return WillPopScope(
      onWillPop: () async {
        bloc.dispose();
        return true;
      },
      child: Scaffold(
        appBar: baseHeader(
          context: context,
          title: 'Transaksi Member',
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: black,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          actions: [
            IconButton(
              icon: Icon(
                Icons.search,
                color: black,
              ),
              onPressed: () {
                showSearch(
                  context: context,
                  delegate: TransactionMemberSearch(),
                );
              },
            ),
          ],
        ),
        body: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: StreamBuilder<List<Item>>(
                  stream: db.itemDao.getAllItem,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return Container(
                        child: CircularProgressIndicator(),
                      );
                    } else if (snapshot.data.length == 0) {
                      return Container(
                        child: Center(
                          child: heading3(text: 'Belum Ada Barang'),
                        ),
                      );
                    }
                    return StaggeredGridView.countBuilder(
                      physics: BouncingScrollPhysics(),
                      crossAxisCount: 4,
                      itemCount: snapshot.data.length,
                      itemBuilder: (BuildContext context, int index) => GestureDetector(
                        onTap: () {
                          // transactionBloc.addItem(snapshot.data[index]);
                          bloc.addItem(snapshot.data[index]);
                        },
                        child: BoxForItem(
                          item: snapshot.data[index],
                        ),
                      ),
                      staggeredTileBuilder: (int index) => new StaggeredTile.count(2, 3),
                      mainAxisSpacing: 8.0,
                      crossAxisSpacing: 8.0,
                    );
                  },
                ),
              )
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: blue,
          onPressed: () {
            bloc.memberBuyer == null
                ? Toast.show('Tambah Member Dulu', context)
                : Navigator.of(context).pushNamed(cartMember);
          },
          child: StreamBuilder<List<Item>>(
            stream: bloc.listItem,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Icon(
                  Icons.shopping_cart,
                  color: white,
                );
              } else if (snapshot.data.length != 0) {
                return Badge(
                  badgeContent: heading6(text: '${snapshot.data.length}', color: white),
                  badgeColor: yellow,
                  position: BadgePosition.bottomLeft(bottom: 12.0),
                  child: Icon(
                    Icons.shopping_cart,
                    color: white,
                  ),
                );
              }
              return Icon(
                Icons.shopping_cart,
                color: white,
              );
            },
          ),
        ),
      ),
    );
  }
}
