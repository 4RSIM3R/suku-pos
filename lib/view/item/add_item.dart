import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:toast/toast.dart';
import 'package:toko_ku/bloc/local/validator.dart';
import 'package:toko_ku/component/clickable.dart';
import 'package:toko_ku/component/header.dart';
import 'package:toko_ku/component/input.dart';
import 'package:toko_ku/constant/constant.dart';
import 'package:toko_ku/constant/routing.dart';
import 'package:toko_ku/service/database/core_database.dart';

class AddItem extends StatefulWidget {
  const AddItem({Key key}) : super(key: key);

  @override
  _AddItemState createState() => _AddItemState();
}

class _AddItemState extends State<AddItem> {
  int id;
  String name, barcode;
  String unit;
  String snapshotItem;
  List<String> itemUnit = [];
  int priceLevel1,
      priceLevel2,
      priceLevel3,
      minPriceLevel1,
      minPriceLevel2,
      minPriceLevel3,
      discount,
      price,
      stock;

  @override
  Widget build(BuildContext context) {
    final db = Provider.of<CoreDatabase>(context);
    final inputBloc = Provider.of<InputBloc>(context);
    return Scaffold(
      appBar: baseHeader(
        context: context,
        title: 'Tambah barang',
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: black,
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.settings,
              color: black,
            ),
            onPressed: () => Navigator.of(context).pushNamed(addItemSetting),
          )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 12.0, left: 12.0, right: 12.0),
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            children: <Widget>[
              Container(
                width: double.infinity,
                padding: EdgeInsets.all(12.0),
                decoration: BoxDecoration(
                  color: white,
                  borderRadius: BorderRadius.circular(5.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black12,
                      blurRadius: 3.0,
                      offset: Offset(4, 4),
                    ),
                  ],
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    StreamBuilder(
                        stream: inputBloc.input,
                        builder: (context, snapshot) {
                          return TextInputWithTitle(
                            title: 'Nama Barang',
                            errorText:
                                snapshot.hasError ? snapshot.error : null,
                            onChanged: (String val) {
                              name = val;
                              inputBloc.changeInput(val);
                            },
                          );
                        }),
                    SizedBox(
                      height: 12.0,
                    ),
                    StreamBuilder(
                        stream: inputBloc.input,
                        builder: (context, snapshot) {
                          return TextInputWithTitle(
                            title: 'Barcode Barang',
                            errorText:
                                snapshot.hasError ? snapshot.error : null,
                            onChanged: (String val) {
                              barcode = val;
                              inputBloc.changeInput(val);
                            },
                          );
                        }),
                    SizedBox(
                      height: 12.0,
                    ),
                    StreamBuilder<List<ItemUnit>>(
                      stream: db.itemUnitDao.getAllItemUnit,
                      builder: (context, snapshot) {
                        if (snapshot.data.length != 0) {
                          itemUnit.clear();
                          snapshot.data.forEach((x) => itemUnit.add(x.name));
                          snapshotItem = snapshot.data[0].name;
                          return BaseDropdown(
                            list: itemUnit,
                            title: 'Satuan Barang',
                            value: unit ?? snapshot.data[0].name,
                            onChanged: (String val) {
                              setState(() => unit = val);
                            },
                            hint: Text('Satuan'),
                          );
                        } else {
                          return BaseDropdown(
                            list: ['Kilogram', 'Pcs'],
                            title: 'Satuan Barang',
                            value: unit,
                            onChanged: (String val) {
                              setState(() => unit = val);
                            },
                            hint: Text('Satuan'),
                          );
                        }
                      },
                    ),
                    SizedBox(
                      height: 12.0,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: StreamBuilder(
                              stream: inputBloc.input,
                              builder: (context, snapshot) {
                                return TextInputWithTitle(
                                  title: 'Harga Level 1',
                                  errorText:
                                      snapshot.hasError ? snapshot.error : null,
                                  onChanged: (String val) {
                                    priceLevel1 = int.parse(val);
                                    inputBloc.changeInput(val);
                                  },
                                  inputType: TextInputType.number,
                                );
                              }),
                        ),
                        SizedBox(
                          width: 12.0,
                        ),
                        Expanded(
                          flex: 1,
                          child: StreamBuilder<Object>(
                              stream: inputBloc.input,
                              builder: (context, snapshot) {
                                return TextInputWithTitle(
                                  title: 'Minimal',
                                  errorText:
                                      snapshot.hasError ? snapshot.error : null,
                                  onChanged: (String val) {
                                    minPriceLevel1 = int.parse(val);
                                    inputBloc.changeInput(val);
                                  },
                                  inputType: TextInputType.number,
                                );
                              }),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 12.0,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: StreamBuilder(
                              stream: inputBloc.input,
                              builder: (context, snapshot) {
                                return TextInputWithTitle(
                                  title: 'Harga Level 2',
                                  errorText:
                                      snapshot.hasError ? snapshot.error : null,
                                  onChanged: (String val) {
                                    priceLevel2 = int.parse(val);
                                    inputBloc.changeInput(val);
                                  },
                                  inputType: TextInputType.number,
                                );
                              }),
                        ),
                        SizedBox(
                          width: 12.0,
                        ),
                        Expanded(
                          flex: 1,
                          child: StreamBuilder(
                              stream: inputBloc.input,
                              builder: (context, snapshot) {
                                return TextInputWithTitle(
                                  title: 'Minimal',
                                  errorText:
                                      snapshot.hasError ? snapshot.error : null,
                                  onChanged: (String val) {
                                    minPriceLevel2 = int.parse(val);
                                    inputBloc.changeInput(val);
                                  },
                                  inputType: TextInputType.number,
                                );
                              }),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 12.0,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: StreamBuilder(
                              stream: inputBloc.input,
                              builder: (context, snapshot) {
                                return TextInputWithTitle(
                                  title: 'Harga Level 3',
                                  errorText:
                                      snapshot.hasError ? snapshot.error : null,
                                  onChanged: (String val) {
                                    priceLevel3 = int.parse(val);
                                    inputBloc.changeInput(val);
                                  },
                                  inputType: TextInputType.number,
                                );
                              }),
                        ),
                        SizedBox(
                          width: 12.0,
                        ),
                        Expanded(
                          flex: 1,
                          child: StreamBuilder(
                              stream: inputBloc.input,
                              builder: (context, snapshot) {
                                return TextInputWithTitle(
                                  title: 'Minimal',
                                  errorText:
                                      snapshot.hasError ? snapshot.error : null,
                                  onChanged: (String val) {
                                    minPriceLevel3 = int.parse(val);
                                    inputBloc.changeInput(val);
                                  },
                                  inputType: TextInputType.number,
                                );
                              }),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 12.0,
                    ),
                    StreamBuilder(
                        stream: inputBloc.input,
                        builder: (context, snapshot) {
                          return TextInputWithTitle(
                            title: 'Diskon Umum',
                            errorText:
                                snapshot.hasError ? snapshot.error : null,
                            onChanged: (String val) {
                              discount = int.parse(val);
                              inputBloc.changeInput(val);
                            },
                            inputType: TextInputType.number,
                          );
                        }),
                    SizedBox(
                      height: 12.0,
                    ),
                    StreamBuilder(
                      stream: inputBloc.input,
                      builder: (context, snapshot) {
                        return TextInputWithTitle(
                          title: 'Harga Beli',
                          errorText:
                          snapshot.hasError ? snapshot.error : null,
                          onChanged: (String val) {
                            price = int.parse(val);
                            inputBloc.changeInput(val);
                          },
                          inputType: TextInputType.number,
                        );
                      }
                    ),
                    SizedBox(
                      height: 12.0,
                    ),
                    StreamBuilder<Object>(
                      stream: inputBloc.input,
                      builder: (context, snapshot) {
                        return TextInputWithTitle(
                          title: 'Sisa Stok',
                          errorText:
                          snapshot.hasError ? snapshot.error : null,
                          onChanged: (String val) {
                            stock = int.parse(val);
                            inputBloc.changeInput(val);
                          },
                          inputType: TextInputType.number,
                        );
                      }
                    ),
                    SizedBox(
                      height: 12.0,
                    ),
                    BaseButton(
                      title: 'Tambah Barang',
                      onClick: () {
                        db.itemDao
                            .addItem(
                          Item(
                            id: id,
                            name: name,
                            barcode: barcode,
                            unit: unit ?? snapshotItem,
                            priceLevel1: priceLevel1,
                            priceLevel2: priceLevel2,
                            priceLevel3: priceLevel3,
                            minPriceLevel1: minPriceLevel1,
                            minPriceLevel2: minPriceLevel2,
                            minPriceLevel3: minPriceLevel3,
                            price: price,
                            discount: discount,
                            stock: stock,
                          ),
                        )
                            .then((val) {
                          Toast.show('Tambah Barang Sukses', context,
                              duration: Toast.LENGTH_SHORT,
                              gravity: Toast.BOTTOM);
                          Navigator.of(context).pop();
                        }).catchError((e) {
                          print('Error : $e');
                          Toast.show('Ada Error, Hubungi Developer', context,
                              duration: Toast.LENGTH_LONG,
                              gravity: Toast.BOTTOM);
                          Navigator.of(context).pop();
                        });
                      },
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
