import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:provider/provider.dart';
import 'package:toko_ku/component/box.dart';
import 'package:toko_ku/component/header.dart';
import 'package:toko_ku/component/typhography.dart';
import 'package:toko_ku/constant/constant.dart';
import 'package:toko_ku/constant/routing.dart';
import 'package:toko_ku/service/database/core_database.dart';
import 'package:toko_ku/utils/search_delegate/item_search_delegate.dart';

class ItemManagement extends StatelessWidget {
  ItemManagement({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final db = Provider.of<CoreDatabase>(context);

    return Scaffold(
      backgroundColor: white,
      appBar: baseHeader(
        context: context,
        title: 'Manajamen Barang',
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: black,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.search,
              color: black,
            ),
            onPressed: () {
              showSearch(
                context: context,
                delegate: ItemSearchDelegate(),
              );
            },
          )
        ],
      ),
      body: StreamBuilder<List<Item>>(
          stream: db.itemDao.getAllItem ?? 0,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            } else if (snapshot.data.length == 0) {
              return Container(
                child: Center(
                  child: heading3(text: 'Belum Ada Barang'),
                ),
              );
            }
            return Padding(
              padding: EdgeInsets.all(12.0),
              child: StaggeredGridView.countBuilder(
                physics: BouncingScrollPhysics(),
                crossAxisCount: 4,
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int index) => BoxForItem(item: snapshot.data[index]),
                staggeredTileBuilder: (int index) => new StaggeredTile.count(2, 3),
                mainAxisSpacing: 8.0,
                crossAxisSpacing: 8.0,
              ),
            );
          }),
      floatingActionButton: FloatingActionButton(
        backgroundColor: blue,
        child: Center(
          child: Icon(
            Icons.add,
            color: white,
          ),
        ),
        onPressed: () => Navigator.of(context).pushNamed(addItem),
      ),
    );
  }
}
