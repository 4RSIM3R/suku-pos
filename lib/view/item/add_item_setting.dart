import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:toast/toast.dart';
import 'package:toko_ku/bloc/local/validator.dart';
import 'package:toko_ku/component/header.dart';
import 'package:toko_ku/component/input.dart';
import 'package:toko_ku/component/typhography.dart';
import 'package:toko_ku/constant/constant.dart';
import 'package:toko_ku/service/database/core_database.dart';
import 'package:toko_ku/utils/show_dialog.dart';

class AddItemSetting extends StatelessWidget {
  AddItemSetting({Key key}) : super(key: key);

  int id;
  String addItemUnit = '';

  @override
  Widget build(BuildContext context) {
    final db = Provider.of<CoreDatabase>(context);
    final inputBloc = Provider.of<InputBloc>(context);
    return Scaffold(
      appBar: baseHeader(
        context: context,
        title: 'Pengaturan tambah barang',
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: black,
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Container(
        margin: EdgeInsets.only(top: 12.0, right: 12.0, left: 12.0),
        child: StreamBuilder<List<ItemUnit>>(
          stream: db.itemUnitDao.getAllItemUnit,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            } else if (snapshot.data.length == 0) {
              return Container(
                child: Center(
                  child: heading3(text: 'Belum Ada Satuan Item'),
                ),
              );
            }
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (context, index) {
                return Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                  margin: EdgeInsets.only(bottom: 8.0),
                  height: 70.0,
                  decoration: BoxDecoration(
                      color: white,
                      borderRadius: BorderRadius.circular(2.0),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black12,
                            offset: Offset(3, 3),
                            blurRadius: 3)
                      ]),
                  width: double.infinity,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: 50.0,
                        width: 50.0,
                        decoration: BoxDecoration(
                          color: yellow,
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        child: Center(
                          child: Icon(
                            MdiIcons.weightKilogram,
                            color: white,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 8.0,
                      ),
                      Expanded(
                        flex: 1,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            heading4(text: snapshot.data[index].name),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 8.0,
                      ),
                      IconButton(
                        icon: Icon(
                          Icons.delete,
                          color: grey,
                          size: 24.0,
                        ),
                        onPressed: () => showCommonDialog(
                          context: context,
                          title: Text('Halo'),
                          content:
                              Text('Apakah anda akan mengahapus satuan ini ?'),
                          actions: [
                            FlatButton(
                              onPressed: () => Navigator.of(context).pop(),
                              child: Text('Tidak'),
                            ),
                            FlatButton(
                              onPressed: () {
                                db.itemUnitDao
                                    .deleteItemUnit(snapshot.data[index]);
                                Navigator.of(context).pop();
                              },
                              child: Text('Iya'),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                );
              },
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        backgroundColor: blue,
        icon: Icon(
          Icons.add,
          color: white,
        ),
        label: heading5(text: 'Tambah', color: white),
        onPressed: () => showCommonDialog(
          context: context,
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              StreamBuilder<String>(
                  stream: inputBloc.input,
                  builder: (context, snapshot) {
                    return TextInputWithTitle(
                      title: 'Satuan Unit',
                      errorText: snapshot.hasError ? snapshot.error : null,
                      onChanged: (String val) {
                        addItemUnit = val;
                        inputBloc.changeInput(val);
                      },
                    );
                  }),
            ],
          ),
          title: Text('Tambah Satuan'),
          actions: [
            FlatButton(
              onPressed: () {
                db.itemUnitDao
                    .addItemUnit(
                  ItemUnit(id: id, name: addItemUnit),
                )
                    .then((val) {
                  Toast.show(
                    'Tambah Satuan berhasil',
                    context,
                    gravity: Toast.BOTTOM,
                  );
                  Navigator.of(context).pop();
                }).catchError((e) {
                  Toast.show(
                    'Ada Error, Harap Hubungi developer',
                    context,
                    gravity: Toast.BOTTOM,
                  );
                });
              },
              child: Text('Simpan'),
            ),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
