import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:toast/toast.dart';
import 'package:toko_ku/component/box.dart';
import 'package:toko_ku/component/header.dart';
import 'package:toko_ku/constant/constant.dart';
import 'package:toko_ku/view/widgets/home_drawer.dart';

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  GlobalKey<ScaffoldState> _drawerKey = GlobalKey();

  DateTime backbuttonpressedTime;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
        key: _drawerKey,
        backgroundColor: white,
        appBar: baseHeader(
            context: context,
            title: 'Beranda',
            leading: IconButton(
              icon: Icon(
                Icons.menu,
                color: black,
              ),
              onPressed: () {
                _drawerKey.currentState.openDrawer();
              },
            ),
            centerTitle: true),
        drawer: HomeDrawer(),
        body: Padding(
          padding: EdgeInsets.all(12.0),
          child: StaggeredGridView.countBuilder(
            physics: BouncingScrollPhysics(),
            crossAxisCount: 4,
            itemCount: litsTitleMenu.length,
            itemBuilder: (BuildContext context, int index) => BoxForList(
              title: litsTitleMenu[index],
              icon: listIconMenu[index],
              index: index,
            ),
            staggeredTileBuilder: (int index) => new StaggeredTile.count(2, index.isEven ? 3 : 2),
            mainAxisSpacing: 4.0,
            crossAxisSpacing: 4.0,
          ),
        ),
      ),
    );
  }

  Future<bool> onWillPop() async {
    DateTime currentTime = DateTime.now();
    //Statement 1 Or statement2
    bool backButton =
        backbuttonpressedTime == null || currentTime.difference(backbuttonpressedTime) > Duration(seconds: 3);
    if (backButton) {
      backbuttonpressedTime = currentTime;
      Toast.show('Tekan 2X untuk keluar', context);
      return Future.value(false);
    }
    SystemChannels.platform.invokeMethod('SystemNavigator.pop');
    return false;
  }
}
