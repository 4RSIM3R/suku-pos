import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:toko_ku/component/clickable.dart';
import 'package:toko_ku/component/input.dart';
import 'package:toko_ku/component/typhography.dart';
import 'package:toko_ku/constant/constant.dart';
import 'package:toko_ku/constant/routing.dart';

class License extends StatelessWidget {
  const License({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => SystemNavigator.pop(),
      child: Scaffold(
        backgroundColor: white,
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 24.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Center(
                  child: Image.asset(
                    "assets/images/license.jpg",
                    fit: BoxFit.fill,
                    height: 275,
                    width: 275,
                  ),
                ),
                SizedBox(
                  height: 24.0,
                ),
                heading2(text: 'Hai, Selamat Datang'),
                SizedBox(
                  height: 12.0,
                ),
                heading5(
                  text: 'Hanya dengan sekali registrasi\nNikmati segala kemudahan.',
                  textAlign: TextAlign.center,
                  fontWeight: FontWeight.w400,
                ),
                SizedBox(
                  height: 24.0,
                ),
                BaseTextInput(
                  hintText: 'Username',
                  prefixIcon: Icon(
                    Icons.person,
                    color: blue,
                  ),
                ),SizedBox(
                  height: 24.0,
                ),
                BaseTextInput(
                  hintText: 'Kode Lisensi',
                  obsecure: true,
                  prefixIcon: Icon(
                    MdiIcons.key,
                    color: blue,
                  ),
                ),
                SizedBox(
                  height: 32.0,
                ),
                baseButton(
                  title: 'Verfikasi',
                  onClick: () => Navigator.of(context).pushNamed(homePage),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
