import 'dart:async';

import 'package:flutter/material.dart';
import 'package:toko_ku/component/typhography.dart';
import 'package:toko_ku/constant/constant.dart';
import 'package:toko_ku/constant/routing.dart';

class Splash extends StatefulWidget {
  const Splash({Key key}) : super(key: key);

  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 2), () => Navigator.of(context).pushNamed(licensePage));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              "assets/images/wave.jpg",
              fit: BoxFit.cover,
              height: 250,
              width: 250,
            ),
            heading1(text: 'Suku Pos'),
            heading5(text: 'Friend for your retail')
          ],
        ),
      ),
    );
  }
}
