import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:toko_ku/component/header.dart';
import 'package:toko_ku/component/typhography.dart';
import 'package:toko_ku/constant/constant.dart';
import 'package:toko_ku/constant/routing.dart';
import 'package:toko_ku/service/database/core_database.dart';
import 'package:toko_ku/utils/show_dialog.dart';

class SettingPoint extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final db = Provider.of<CoreDatabase>(context);
    return Scaffold(
      appBar: baseHeader(
          context: context,
          title: 'Setting Point',
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: black,
            ),
            onPressed: () => Navigator.of(context).pop(),
          )),
      body: Padding(
        padding: EdgeInsets.all(12.0),
        child: StreamBuilder<List<PointSetting>>(
          stream: db.pointSettingDao.getAllPointSettings,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Container(
                child: CircularProgressIndicator(),
              );
            } else if (snapshot.data.length == 0) {
              return Container(
                child: Center(
                  child: heading3(text: 'Belum Ada Poin'),
                ),
              );
            }
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (context, index) {
                return Container(
                  padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                  margin: EdgeInsets.only(bottom: 8.0),
                  height: 70.0,
                  decoration: BoxDecoration(
                      color: white,
                      borderRadius: BorderRadius.circular(2.0),
                      boxShadow: [BoxShadow(color: Colors.black12, offset: Offset(3, 3), blurRadius: 3)]),
                  width: double.infinity,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: 50.0,
                        width: 50.0,
                        decoration: BoxDecoration(color: yellow, borderRadius: BorderRadius.circular(5.0)),
                      ),
                      SizedBox(
                        width: 8.0,
                      ),
                      Expanded(
                        flex: 1,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            heading4(text: snapshot.data[index].minTransaction.toString()),
                            heading5(text: '${snapshot.data[index].pointAwarded} poin'),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 8.0,
                      ),
                      IconButton(
                        icon: Icon(
                          Icons.delete,
                          color: red,
                          size: 24.0,
                        ),
                        onPressed: () => showCommonDialog(
                          context: context,
                          content: Text('Apakah anda yakin menghapus seting poin ini ?'),
                          title: Text('Hai'),
                          actions: [
                            FlatButton(onPressed: () => Navigator.of(context).pop(), child: Text('Tidak')),
                            FlatButton(
                              onPressed: () => db.pointSettingDao.deletePointSetting(snapshot.data[index]),
                              child: Text('Iya'),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                );
              },
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        backgroundColor: blue,
        icon: Icon(
          Icons.add,
          color: white,
        ),
        label: heading5(text: 'Tambah', color: white),
        onPressed: () {
          Navigator.of(context).pushNamed(addPoint);
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
