import 'package:flutter/material.dart';
import 'package:toko_ku/component/header.dart';
import 'package:toko_ku/constant/constant.dart';
import 'package:toko_ku/view/widgets/form_add_point.dart';

class AddPoint extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      appBar: baseHeader(
          context: context,
          title: 'Tambah Point',
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: black,
            ),
            onPressed: () => Navigator.of(context).pop(),
          )),
      body: Container(
        margin: EdgeInsets.all(12.0),
        padding: EdgeInsets.all(12.0),
        decoration: BoxDecoration(color: white, boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 3.0,
            offset: Offset(4, 4),
          ),
        ]),
        child: FormAddPoint(),
      ),
    );
  }
}
