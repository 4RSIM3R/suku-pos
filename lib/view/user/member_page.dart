import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:provider/provider.dart';
import 'package:stopper/stopper.dart';
import 'package:toast/toast.dart';
import 'package:toko_ku/component/clickable.dart';
import 'package:toko_ku/component/typhography.dart';
import 'package:toko_ku/constant/constant.dart';
import 'package:toko_ku/constant/routing.dart';
import 'package:toko_ku/service/database/core_database.dart';
import 'package:toko_ku/utils/show_dialog.dart';
import 'package:toko_ku/view/widgets/edit_member.dart';
import 'package:toko_ku/view/widgets/member_item.dart';

class MemberPage extends StatelessWidget {
  const MemberPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final db = Provider.of<CoreDatabase>(context);

    return Container(
      margin: EdgeInsets.only(top: 12.0, right: 12.0, left: 12.0),
      child: StreamBuilder<List<Member>>(
        stream: db.memberDao.getAllMember ?? 0,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
          } else if (snapshot.data.length == 0) {
            return Container(
              child: Center(
                child: heading3(text: 'Belum Ada Member'),
              ),
            );
          }
          return ListView.builder(
            itemCount: snapshot.data.length ?? 0,
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () {
                  showDetail(snapshot.data[index], context);
                },
                child: Slidable(
                  actionPane: SlidableDrawerActionPane(),
                  child: MemberItem(
                    member: snapshot.data[index],
                    type: "member",
                  ),
                  actions: <Widget>[
                    Container(
                      height: 70.0,
                      width: 70.0,
                      margin: EdgeInsets.only(bottom: 6.0),
                      child: IconSlideAction(
                        caption: 'Hapus',
                        color: red,
                        icon: Icons.delete,
                        onTap: () => showCommonDialog(
                          context: context,
                          title: heading4(text: 'Hai'),
                          content: heading5(text: 'Apakah anda yakin menghapus member ini'),
                          actions: [
                            FlatButton(
                                onPressed: () => Navigator.of(context).pop(), child: heading6(text: 'Tidak')),
                            FlatButton(
                              onPressed: () {
                                db.memberDao.deleteMember(snapshot.data[index]).then((val) {
                                  Navigator.of(context).pop();
                                }).catchError((e) {
                                  Toast.show(
                                    'Ada Error, Hubungi developer',
                                    context,
                                  );
                                });
                              },
                              child: heading6(text: 'Iya'),
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 70.0,
                      width: 70.0,
                      margin: EdgeInsets.only(bottom: 6.0),
                      child: IconSlideAction(
                        caption: 'Edit',
                        color: Colors.green,
                        icon: Icons.edit,
                        onTap: () => Navigator.of(context).pushNamed(
                          editMember,
                          arguments: EditMemberArgument(
                            address: snapshot.data[index].address,
                            identityNumber: snapshot.data[index].identityNumber,
                            name: snapshot.data[index].name,
                            identityType: snapshot.data[index].identityType,
                            memberNumber: snapshot.data[index].memberNumber,
                            phone: snapshot.data[index].phone,
                            type: snapshot.data[index].type,
                            id: snapshot.data[index].id,
                            totalPoint: snapshot.data[index].totalPoint
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              );
            },
          );
        },
      ),
    );
  }

  void showDetail(Member member, BuildContext context) {
    showStopper(
      context: context,
      builder: (context, scrollController, scrollPhysics, stop) {
        return ListView(
          controller: scrollController,
          physics: scrollPhysics,
          scrollDirection: Axis.vertical,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(18.0),
              decoration: BoxDecoration(color: white),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  heading2(text: member.name),
                  SizedBox(
                    height: 12.0,
                  ),
                  Container(
                    height: 2.0,
                    width: double.infinity,
                    decoration: BoxDecoration(color: Colors.grey[300]),
                  ),
                  SizedBox(
                    height: 12.0,
                  ),
                  heading5(text: 'Member ID : ${member.memberNumber}'),
                  SizedBox(
                    height: 12.0,
                  ),
                  heading5(text: 'Alamat : ${member.address}'),
                  SizedBox(
                    height: 12.0,
                  ),
                  heading5(text: 'Tipe Identitas : ${member.identityType} | ${member.identityNumber}'),
                  SizedBox(
                    height: 12.0,
                  ),
                  heading5(text: 'Tipe Member : ${member.type}'),
                  SizedBox(
                    height: 12.0,
                  ),
                  BaseButton(title: 'Telepon', onClick: () {}),
                  SizedBox(
                    height: 12.0,
                  ),
                ],
              ),
            )
          ],
        );
      },
      stops: [0.4 * MediaQuery.of(context).size.height, 0.5 * MediaQuery.of(context).size.height],
    );
  }
}
