import 'package:flutter/material.dart';
import 'package:toko_ku/component/typhography.dart';
import 'package:toko_ku/constant/constant.dart';

class UserItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
          horizontal: 10.0,
          vertical: 10.0
      ),
      margin: EdgeInsets.only(
          bottom: 8.0
      ),
      height: 70.0,
      decoration: BoxDecoration(
          color: white,
          borderRadius: BorderRadius.circular(5.0),
          boxShadow: [
            BoxShadow(
                color: Colors.black12,
                offset: Offset(3, 3),
                blurRadius: 3
            )
          ]
      ),
      width: double.infinity,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            height: 50.0,
            width: 50.0,
            decoration: BoxDecoration(
                color: yellow,
                borderRadius: BorderRadius.circular(5.0)
            ),
          ),
          SizedBox(width: 8.0,),
          Expanded(
            flex: 1,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                heading4(text: 'Suku Alkodingi'),
              ],
            ),
          ),
          SizedBox(width: 8.0,),
          IconButton(icon: Icon(Icons.delete, color: grey, size: 24.0,), onPressed: (){})
        ],
      ),
    );
  }
}
