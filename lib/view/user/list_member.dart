import 'package:flutter/material.dart';
import 'package:toko_ku/component/typhography.dart';
import 'package:toko_ku/constant/constant.dart';
import 'package:toko_ku/constant/routing.dart';
import 'package:toko_ku/utils/search_delegate/member_search_delegate.dart';
import 'package:toko_ku/utils/search_delegate/supplier_search_delegate.dart';
import 'package:toko_ku/view/user/add_user.dart';
import 'package:toko_ku/view/user/member_page.dart';
import 'package:toko_ku/view/user/supplier_page.dart';

class ListMember extends StatefulWidget {
  ListMember({Key key}) : super(key: key);

  @override
  _ListMemberState createState() => _ListMemberState();
}

class _ListMemberState extends State<ListMember> with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    _tabController = new TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: white,
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: black,
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: heading4(text: 'List Member Dan Supplier'),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.search,
              color: black,
            ),
            onPressed: () {
              showSearch(
                context: context,
                delegate: _tabController.index == 0 ? MemberSearchDelegate() : SupplierSearchDelegate(),
              );
            },
          ),
        ],
        bottom: TabBar(
          controller: _tabController,
          unselectedLabelColor: Colors.black12,
          labelColor: blue,
          tabs: [
            Tab(
              child: heading5(text: 'Member'),
            ),
            Tab(
              child: heading5(text: 'Supplier'),
            )
          ],
        ),
      ),
      body: TabBarView(
        controller: _tabController,
        physics: BouncingScrollPhysics(),
        children: [
          MemberPage(),
          SupplierPage(),
        ],
      ),
      floatingActionButton: FloatingActionButton.extended(
        backgroundColor: blue,
        icon: Icon(
          Icons.add,
          color: white,
        ),
        label: heading5(text: 'Tambah', color: white),
        onPressed: () {
          Navigator.of(context).pushNamed(
            addUser,
            arguments: AddUserArgument(tabIndex: _tabController.index),
          );
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
