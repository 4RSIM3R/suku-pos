import 'package:flutter/material.dart';
import 'package:toko_ku/component/header.dart';
import 'package:toko_ku/constant/constant.dart';
import 'package:toko_ku/view/widgets/form_add_member.dart';
import 'package:toko_ku/view/widgets/form_add_supplier.dart';

class AddUser extends StatelessWidget {
  final AddUserArgument argument;

  AddUser({Key key, @required this.argument}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: baseHeader(
        context: context,
        title: argument.tabIndex == 0 ? 'Tambah Member' : 'Tambah Supplier',
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: black,
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 12.0, left: 12.0, right: 12.0),
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            children: <Widget>[
              Container(
                width: double.infinity,
                padding: EdgeInsets.all(12.0),
                decoration: BoxDecoration(
                  color: white,
                  borderRadius: BorderRadius.circular(5.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black12,
                      blurRadius: 3.0,
                      offset: Offset(4, 4),
                    ),
                  ],
                ),
                child: argument.tabIndex == 0
                    ? FormAddMember()
                    : FormAddSupplier(),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class AddUserArgument {
  final int tabIndex;

  AddUserArgument({@required this.tabIndex});
}
