import 'package:flutter/material.dart';
import 'package:toko_ku/component/header.dart';
import 'package:toko_ku/constant/constant.dart';
import 'package:toko_ku/constant/routing.dart';
import 'package:toko_ku/view/user/user_item.dart';

class ManageUser extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: baseHeader(
          context: context,
          title: "Manager User",
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: black,
            ),
            onPressed: () => Navigator.of(context).pop(),
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.exit_to_app,
                color: red,
              ),
              onPressed: () => Navigator.of(context).pop(),
            )
          ]),
      floatingActionButton: FloatingActionButton(
        backgroundColor: red,
        child: Icon(
          Icons.add,
          color: white,
        ),
        onPressed: () {
          Navigator.of(context).pushNamed(addUser);
        },
      ),
      body: Container(
        child: SingleChildScrollView(
          padding: EdgeInsets.all(12.0),
          physics: BouncingScrollPhysics(),
          child: Column(
            children: <Widget>[
              UserItem(),
              UserItem(),
              UserItem(),
            ],
          ),
        ),
      ),
    );
  }
}
