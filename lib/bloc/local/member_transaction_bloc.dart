import 'package:rxdart/rxdart.dart';
import 'package:toko_ku/service/database/core_database.dart';

class MemberTransactionBloc {
  static final _listItem = BehaviorSubject<List<Item>>.seeded([]);
  static final _listItemUnit = BehaviorSubject<List<int>>.seeded([1]);
  static final _priceLevel = BehaviorSubject<List<int>>.seeded([0]);
  static final _levelPricePerUnit = BehaviorSubject<List<int>>.seeded([]);
  static final _priceTotal = BehaviorSubject<int>.seeded(0);
  static final _chargeUser = BehaviorSubject<int>.seeded(0);
  // false = half | true = full
  static final _halfOrFull = BehaviorSubject<List<bool>>.seeded([]);
  static final _member = BehaviorSubject<Member>();

  // Get all value of stream
  List<Item> listItemValue = _listItem.value;
  List<int> listItemUnitValue = _listItemUnit.value;
  List<int> priceLevelValue = _priceLevel.value;
  List<int> levelPricePerUnitValue = _levelPricePerUnit.value;
  int priceTotalValue = _priceTotal.value;
  int chargeUserValue = _chargeUser.value;
  Member memberValue = _member.value;

  void initCartPage() {
    List<int> listOfUnit = _listItemUnit.value;
    List<int> listOfPrice = _priceLevel.value;
    List<int> levelPricePerUnit = _levelPricePerUnit.value;
    List<bool> halfOrFull = _halfOrFull.value;
    _listItem.value.forEach((x) => listOfUnit.add(1));
    _listItem.value.forEach((x) => listOfPrice.add(0));
    _listItem.value.forEach((x) => levelPricePerUnit.add(0));
    if (_member.value.type == 'Khusus') {
      _listItem.value.forEach((x) => halfOrFull.add(false));
    } else {
      _listItem.value.forEach((x) => halfOrFull.add(true));
    }
    _listItemUnit.sink.add(listOfUnit);
    _priceLevel.sink.add(listOfPrice);
    _levelPricePerUnit.sink.add(levelPricePerUnit);
  }

  Stream<List<Item>> get listItem => _listItem.stream;

  Stream<List<int>> get listItemUnit => _listItemUnit.stream;


  Stream<List<int>> get priceLevel => _priceLevel.stream;

  Stream<int> get priceTotal => _priceTotal.stream;

  Stream<List<int>> get levelPricePerUnit => _levelPricePerUnit.stream;

  Stream<int> get chargeUser => _chargeUser.stream;

  Stream<Member> get member => _member.stream;

  Stream<List<bool>> get halfOrFull => _halfOrFull.stream;

  Member get memberBuyer => _member.value;

  void addItem(Item item) {
    List<Item> listItem = _listItem.value;
    if (listItem.length != 0) {
      if (!listItem.contains(item)) {
        listItem.add(item);
      }
      _listItem.sink.add(listItem);
    } else {
      listItem.add(item);
      _listItem.sink.add(listItem);
    }
  }

  void addMember(Member member) => _member.sink.add(member);

  void addUnit(int index) {
    List<int> listUnit = _listItemUnit.value;
    listUnit[index] = ++listUnit[index];
    _listItemUnit.sink.add(listUnit);
    checkPriceUnit(index);
    checkPriceTotal();
  }

  void removeUnit(int index) {
    if (_listItemUnit.value[index] >= 0) {
      List<int> listUnit = _listItemUnit.value;
      listUnit[index] = --listUnit[index];
      _listItemUnit.sink.add(listUnit);
      checkPriceUnit(index);
      checkPriceTotal();
    }
  }

  void changeHalfOrFull(int index) {
    List<bool> hof = _halfOrFull.value;
    hof[index] = !hof[index];
    _halfOrFull.sink.add(hof);
    checkPriceUnit(index);
    checkPriceTotal();
  }

  void checkPriceUnit(int index) {
    var item = _listItem.value[index];
    var unit = _listItemUnit.value[index];
    var halfOrFull = _halfOrFull.value[index];

    List<int> listPriceLevel = _priceLevel.value;

    if (unit < item.minPriceLevel3 && unit < item.minPriceLevel2) {
      int priceTotalPerUnit = unit * item.priceLevel1 ~/ (halfOrFull == true ? 1 : 2);
      List<int> listLevelPricePerUnit = _levelPricePerUnit.value;
      listLevelPricePerUnit[index] = item.priceLevel1;
      print('Total Harga Per Unit Sekarang $priceTotalPerUnit');
      listPriceLevel[index] = priceTotalPerUnit;
      _priceLevel.sink.add(listPriceLevel);
      _levelPricePerUnit.sink.add(listLevelPricePerUnit);
    } else if (unit < item.minPriceLevel3 && unit >= item.minPriceLevel2) {
      int priceTotalPerUnit = unit * item.priceLevel2 ~/ (halfOrFull == true ? 1 : 2);
      List<int> listLevelPricePerUnit = _levelPricePerUnit.value;
      print('Total Harga Per Unit Sekarang $priceTotalPerUnit');
      listPriceLevel[index] = priceTotalPerUnit;
      listLevelPricePerUnit[index] = item.priceLevel2;
      _priceLevel.sink.add(listPriceLevel);
      _levelPricePerUnit.sink.add(listLevelPricePerUnit);
    } else {
      int priceTotalPerUnit = unit * item.priceLevel3 ~/ (halfOrFull == true ? 1 : 2);
      print('Total Harga Per Unit Sekarang $priceTotalPerUnit');
      listPriceLevel[index] = priceTotalPerUnit;
      List<int> listLevelPricePerUnit = _levelPricePerUnit.value;
      listLevelPricePerUnit[index] = item.priceLevel3;
      _priceLevel.sink.add(listPriceLevel);
      _levelPricePerUnit.sink.add(listLevelPricePerUnit);
    }
  }

  void checkPriceTotal() {
    int result = _priceLevel.value.reduce((element, value) => element + value);
    print('Total Semua Harga : $result');
    _priceTotal.sink.add(result);
  }

  void userCharge(int buyerMoney) {
    var totalPrice = _priceTotal.value;
    var charge = buyerMoney - totalPrice;
    _chargeUser.sink.add(charge);
  }

  void dispose() {
    _listItem.sink.add([]);
    _listItem.sink.add([]);
    _listItemUnit.sink.add([]);
    _levelPricePerUnit.sink.add([]);
    _priceLevel.sink.add([]);
    _priceTotal.sink.add(0);
    _member.sink.add(null);
  }
}
