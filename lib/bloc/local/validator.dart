import 'dart:async';

import 'package:rxdart/rxdart.dart';

class InputBloc {
  final BehaviorSubject<String> _input = BehaviorSubject<String>();

  Stream<String> get input => _input.stream;

  //dynamic get changeInput => _input.sink.add;
  changeInput(String text) {
    (text == null || text == "") ? _input.sink.addError("Invalid value entered!") : _input.sink.add(text);
  }
}
