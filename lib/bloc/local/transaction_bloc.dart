import 'dart:async';

import 'package:rxdart/rxdart.dart';
import 'package:toko_ku/service/database/core_database.dart';

class TransactionBloc {
  final _listItem = BehaviorSubject<List<Item>>.seeded([]);
  final _listItemUnit = BehaviorSubject<List<int>>.seeded([1]);
  final _priceItemUnit = BehaviorSubject<List<int>>.seeded([]);
  final _priceLevel = BehaviorSubject<List<int>>.seeded([0]);
  final _levelPricePerUnit = BehaviorSubject<List<int>>.seeded([]);
  final _priceTotal = BehaviorSubject<int>.seeded(0);
  final _chargeUser = BehaviorSubject<int>.seeded(0);

  Stream<List<Item>> get listItem => _listItem.stream;

  Stream<List<int>> get listItemUnit => _listItemUnit.stream;

  Stream<List<int>> get priceItemUnit => _priceItemUnit.stream;

  Stream<List<int>> get priceLevel => _priceLevel.stream;

  Stream<int> get priceTotal => _priceTotal.stream;

  Stream<List<int>> get levelPricePerUnit => _levelPricePerUnit.stream;

  Stream<int> get chargeUser => _chargeUser.stream;

  void addItem(Item item) {
    List<Item> listItem = _listItem.value;
    if (listItem.length != 0) {
      if (!listItem.contains(item)) {
        listItem.add(item);
      }
      _listItem.sink.add(listItem);
    } else {
      listItem.add(item);
      _listItem.sink.add(listItem);
    }
  }

  void initCartPage() {
    List<int> listOfUnit = _listItemUnit.value;
    List<int> listOfPrice = _priceLevel.value;
    List<int> levelPricePerUnit = _levelPricePerUnit.value;
    _listItem.value.forEach((x) => listOfUnit.add(1));
    _listItem.value.forEach((x) => listOfPrice.add(0));
    _listItem.value.forEach((x) => levelPricePerUnit.add(0));
    _listItemUnit.sink.add(listOfUnit);
    _priceLevel.sink.add(listOfPrice);
    _levelPricePerUnit.sink.add(levelPricePerUnit);
  }

  void addUnit(int index) {
    List<int> listUnit = _listItemUnit.value;
    listUnit[index] = ++listUnit[index];
    _listItemUnit.sink.add(listUnit);
    checkPriceUnit(index);
    checkPriceTotal();
  }

  void removeUnit(int index) {
    if (_listItemUnit.value[index] >= 0) {
      List<int> listUnit = _listItemUnit.value;
      listUnit[index] = --listUnit[index];
      _listItemUnit.sink.add(listUnit);
      checkPriceUnit(index);
      checkPriceTotal();
    }
  }

  void checkPriceUnit(int index) {
    var item = _listItem.value[index];
    var unit = _listItemUnit.value[index];

    List<int> listPriceLevel = _priceLevel.value;

    if (unit < item.minPriceLevel3 && unit < item.minPriceLevel2) {
      int priceTotalPerUnit = unit * item.priceLevel1;
      List<int> listLevelPricePerUnit = _levelPricePerUnit.value;
      listLevelPricePerUnit[index] = item.priceLevel1;
      print('Total Harga Per Unit Sekarang $priceTotalPerUnit');
      listPriceLevel[index] = priceTotalPerUnit;
      _priceLevel.sink.add(listPriceLevel);
      _levelPricePerUnit.sink.add(listLevelPricePerUnit);
    } else if (unit < item.minPriceLevel3 && unit >= item.minPriceLevel2) {
      int priceTotalPerUnit = unit * item.priceLevel2;
      List<int> listLevelPricePerUnit = _levelPricePerUnit.value;
      print('Total Harga Per Unit Sekarang $priceTotalPerUnit');
      listPriceLevel[index] = priceTotalPerUnit;
      listLevelPricePerUnit[index] = item.priceLevel2;
      _priceLevel.sink.add(listPriceLevel);
      _levelPricePerUnit.sink.add(listLevelPricePerUnit);
    } else {
      int priceTotalPerUnit = unit * item.priceLevel3;
      print('Total Harga Per Unit Sekarang $priceTotalPerUnit');
      listPriceLevel[index] = priceTotalPerUnit;
      List<int> listLevelPricePerUnit = _levelPricePerUnit.value;
      listLevelPricePerUnit[index] = item.priceLevel3;
      _priceLevel.sink.add(listPriceLevel);
      _levelPricePerUnit.sink.add(listLevelPricePerUnit);
    }
  }

  void checkPriceTotal(){
    int result = _priceLevel.value.reduce((element, value) => element + value);
    print('Total Semua Harga : $result');
    _priceTotal.sink.add(result);
  }

  void userCharge(int buyerMoney){
    var totalPrice = _priceTotal.value;
    var charge = buyerMoney - totalPrice;
    _chargeUser.sink.add(charge);
  }

  void dispose() {
    this._listItem.sink.add([]);
    this._listItemUnit.sink.add([]);
    this._levelPricePerUnit.sink.add([]);
    this._priceItemUnit.sink.add([]);
    this._priceLevel.sink.add([]);
    this._priceTotal.sink.add(0);
    this._chargeUser.sink.add(0);
  }
}
